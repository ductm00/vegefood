AOS.init({
    duration: 800,
    easing: 'slide'
});

(function($) {

    "use strict";

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };


    $(window).stellar({
        responsive: true,
        parallaxBackgrounds: true,
        parallaxElements: true,
        horizontalScrolling: false,
        hideDistantElements: false,
        scrollProperty: 'scroll'
    });


    var fullHeight = function() {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function() {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    // loader
    var loader = function() {
        setTimeout(function() {
            if ($('#ftco-loader').length > 0) {
                $('#ftco-loader').removeClass('show');
            }
        }, 1);
    };
    loader();

    // Scrollax
    $.Scrollax();

    var carousel = function() {
        $('.home-slider').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 0,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            nav: false,
            autoplayHoverPause: false,
            items: 1,
            navText: ["<span class='ion-md-arrow-back'></span>", "<span class='ion-chevron-right'></span>"],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $('.carousel-testimony').owlCarousel({
            center: true,
            loop: true,
            items: 1,
            margin: 30,
            stagePadding: 0,
            nav: false,
            navText: ['<span class="ion-ios-arrow-back">', '<span class="ion-ios-arrow-forward">'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });

    };
    carousel();

    $('nav .dropdown').hover(function() {
        var $this = $(this);
        // 	 timer;
        // clearTimeout(timer);
        $this.addClass('show');
        $this.find('> a').attr('aria-expanded', true);
        // $this.find('.dropdown-menu').addClass('animated-fast fadeInUp show');
        $this.find('.dropdown-menu').addClass('show');
    }, function() {
        var $this = $(this);
        // timer;
        // timer = setTimeout(function(){
        $this.removeClass('show');
        $this.find('> a').attr('aria-expanded', false);
        // $this.find('.dropdown-menu').removeClass('animated-fast fadeInUp show');
        $this.find('.dropdown-menu').removeClass('show');
        // }, 100);
    });


    $('#dropdown04').on('show.bs.dropdown', function() {
        console.log('show');
    });

    // scroll
    var scrollWindow = function() {
        $(window).scroll(function() {
            var $w = $(this),
                st = $w.scrollTop(),
                navbar = $('.ftco_navbar'),
                sd = $('.js-scroll-wrap');

            if (st > 150) {
                if (!navbar.hasClass('scrolled')) {
                    navbar.addClass('scrolled');
                }
            }
            if (st < 150) {
                if (navbar.hasClass('scrolled')) {
                    navbar.removeClass('scrolled sleep');
                }
            }
            if (st > 350) {
                if (!navbar.hasClass('awake')) {
                    navbar.addClass('awake');
                }

                if (sd.length > 0) {
                    sd.addClass('sleep');
                }
            }
            if (st < 350) {
                if (navbar.hasClass('awake')) {
                    navbar.removeClass('awake');
                    navbar.addClass('sleep');
                }
                if (sd.length > 0) {
                    sd.removeClass('sleep');
                }
            }
        });
    };
    scrollWindow();


    var counter = function() {

        $('#section-counter').waypoint(function(direction) {

            if (direction === 'down' && !$(this.element).hasClass('ftco-animated')) {

                var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
                $('.number').each(function() {
                    var $this = $(this),
                        num = $this.data('number');
                    console.log(num);
                    $this.animateNumber({
                        number: num,
                        numberStep: comma_separator_number_step
                    }, 7000);
                });

            }

        }, { offset: '95%' });

    }
    counter();

    var contentWayPoint = function() {
        var i = 0;
        $('.ftco-animate').waypoint(function(direction) {

            if (direction === 'down' && !$(this.element).hasClass('ftco-animated')) {

                i++;

                $(this.element).addClass('item-animate');
                setTimeout(function() {

                    $('body .ftco-animate.item-animate').each(function(k) {
                        var el = $(this);
                        setTimeout(function() {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn ftco-animated');
                            } else if (effect === 'fadeInLeft') {
                                el.addClass('fadeInLeft ftco-animated');
                            } else if (effect === 'fadeInRight') {
                                el.addClass('fadeInRight ftco-animated');
                            } else {
                                el.addClass('fadeInUp ftco-animated');
                            }
                            el.removeClass('item-animate');
                        }, k * 50, 'easeInOutExpo');
                    });

                }, 100);

            }

        }, { offset: '95%' });
    };
    contentWayPoint();


    // navigation
    var OnePageNav = function() {
        $(".smoothscroll[href^='#'], #ftco-nav ul li a[href^='#']").on('click', function(e) {
            e.preventDefault();

            var hash = this.hash,
                navToggler = $('.navbar-toggler');
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 700, 'easeInOutExpo', function() {
                window.location.hash = hash;
            });


            if (navToggler.is(':visible')) {
                navToggler.click();
            }
        });
        $('body').on('activate.bs.scrollspy', function() {
            console.log('nice');
        })
    };
    OnePageNav();


    // magnific popup
    $('.image-popup').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });



    var goHere = function() {

        $('.mouse-icon').on('click', function(event) {

            event.preventDefault();

            $('html,body').animate({
                scrollTop: $('.goto-here').offset().top
            }, 500, 'easeInOutExpo');

            return false;
        });
    };
    goHere();


    function makeTimer() {

        var endTime = new Date("21 December 2019 9:56:00 GMT+01:00");
        endTime = (Date.parse(endTime) / 1000);

        var now = new Date();
        now = (Date.parse(now) / 1000);

        var timeLeft = endTime - now;

        var days = Math.floor(timeLeft / 86400);
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }

        $("#days").html(days + "<span>Days</span>");
        $("#hours").html(hours + "<span>Hours</span>");
        $("#minutes").html(minutes + "<span>Minutes</span>");
        $("#seconds").html(seconds + "<span>Seconds</span>");

    }

    setInterval(function() { makeTimer(); }, 1000);

    // update quantity button + -

    var proQty = $('.pro-qty');
    proQty.prepend('<span class="dec qtybtn">-</span>');
    proQty.append('<span class="inc qtybtn">+</span>');
    proQty.on('click', '.qtybtn', function() {

        var price = $(this).closest('.pro-qty').children('.product_price').val();
        var qty = $(this).closest('.pro-qty').children('.new_quantity').val();
        var total = 0;
        var totalQty = 0;
        var $button = $(this);
        var cart_product_id = $button.siblings('.cart_product_id').val();
        var quantity_instock = $button.siblings('.quantity_instock').val();
        var _token = $('meta[name="csrf-token"]').attr('content');

        if ($button.hasClass('inc')) {
            totalQty += parseInt(qty) + 1;


        } else {
            if (qty != 0) {
                totalQty += parseInt(qty) - 1;
                if (totalQty == 0) {
                    $(this).closest('tr').remove();
                }
            }
        }

        $(this).siblings('.quantity_now').val(totalQty);

        var totall = $('.totall').text();
        let subtotal = $('.subtotal').text();

        if ($button.hasClass('inc')) {
            if (totalQty <= quantity_instock) {
                if ($(this).closest('.shoping__cart__quantity').siblings('.check').children().is(':checked')) {

                    $('.totall').text(parseFloat(totall) + parseFloat(price));
                    $('.subtotal').text(parseFloat(subtotal) + parseFloat(price));
                }
            } else {
                $('.totall').text(parseFloat(totall));
                $('.subtotal').text(parseFloat(subtotal));
            }


        } else {

            if ($(this).closest('.shoping__cart__quantity').siblings('.check').children().is(':checked') && (qty != 0)) {

                $('.totall').text(parseFloat(totall) - parseFloat(price));
                $('.subtotal').text(parseFloat(subtotal) - parseFloat(price));
            } else {
                $('.totall').text('0');
                $('.subtotal').text('0');
            }

        }

        if (totalQty > quantity_instock) {
            alert('Chỉ còn ' + quantity_instock + ' sản phẩm trong kho')
            $(this).siblings('.new_quantity').val(quantity_instock);
            totalQty = quantity_instock;
        } else {

            $button.parent().find('.new_quantity').val(totalQty);
        }

        total = price * parseFloat(totalQty);

        $(this).closest('.pro-qty').parent().parent().next().text(total);

        jQuery.ajax({
            url: '/gio-hang/cap-nhat',
            type: 'put',
            data: {
                id: cart_product_id,
                quantity_update: totalQty,
                '_token': _token,
            },
            success: function(response) {
                if (response) {
                    $('.icon-shopping_cart').text('[' + response.quantity_in_cart + ']');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    // on input quantity change

    $('.new_quantity').on('change', function() {

        var quantity_instock = $(this).siblings('.quantity_instock').val();
        var quantity_update = $(this).val();
        var cart_id = $(this).siblings('.cart_product_id').val();
        var _token = $('meta[name="csrf-token"]').attr('content');

        if (parseInt(quantity_update) > parseInt(quantity_instock)) {
            alert('Trong kho chỉ còn ' + quantity_instock + ' sản phẩm')
            $(this).val(quantity_instock);
            quantity_update = quantity_instock;
        }

        if (parseInt(quantity_update) < 0) {
            alert('Số lượng sản phẩm khồng được âm');
            $(this).val('0');
            quantity_update = 0;
        }

        $(this).siblings('.quantity_now').val(quantity_update);

        jQuery.ajax({
            url: '/gio-hang/cap-nhat',
            type: 'put',
            data: {
                id: cart_id,
                quantity_update: quantity_update,
                '_token': _token,
            },
            success: function(response) {
                if (response) {
                    $('.icon-shopping_cart').text('[' + response.quantity_in_cart + ']');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });

        var price = $(this).closest('tr').find('.price').text();

        var total = parseFloat(price) * quantity_update;

        $(this).closest('tr').find('.total').text(total);

        var totall = 0;

        $('.cb-element').each(function() {
            if (this.checked) {
                totall += parseFloat($(this).parent().siblings('.total').text());
            }
        })

        var discount = parseFloat($('.discount').text());

        if ($(this).closest('.shoping__cart__quantity').siblings('.check').children().is(':checked')) {
            $('.subtotal').text(totall);
            $('.totall').text(totall + discount);
        }
    });

    //remove product from cart

    $('.remove-cart').on('click', function() {

        var total = $(this).closest('.product-remove').siblings('.total').text();

        var totall = $('.totall').text();
        let subtotal = $('.subtotal').text();

        var cart_product_id = $(this).parent().find('.cart_product_id').val();
        var _token = $('meta[name="csrf-token"]').attr('content');

        jQuery.ajax({
            url: '/gio-hang/' + cart_product_id,
            type: 'DELETE',
            data: {
                id: cart_product_id,
                '_token': _token,
            },
            success: function(response) {

                $('.icon-shopping_cart').text('[' + response.quantity_in_cart + ']');
            },
            error: function(error) {
                console.log(error);
            }
        });

        if ($(this).closest('.product-remove').siblings('.check').children().is(':checked')) {
            $('.totall').text(parseFloat(totall) - parseFloat(total));
            $('.subtotal').text(parseFloat(subtotal) - parseFloat(total));

        }

        $(this).closest('.product-remove').parent().remove();


    });


    // check coupon

    $('.check-coupon').on('click', function() {

        var coupon_code = $(this).siblings('.coupon_code').val();
        var _token = $('meta[name="csrf-token"]').attr('content');
        let subtotal = $('.subtotal').text();
        var discount_value = 0;



        jQuery.ajax({
            url: '/gio-hang/check-coupon',
            type: 'get',
            data: {
                coupon_code: coupon_code,
                '_token': _token,
            },
            success: function(response) {

                var color = response.coupon_value == '0' ? 'red' : 'green';
                $('.success-coupon').text(response.message).css('color', color);

                discount_value = parseFloat(subtotal) * parseFloat(response.coupon_value);

                $('.discount').text(discount_value + ' VND');

                $('#coupon_value').val(response.coupon_value);
                if (response.coupon_value != '0') {
                    $('#coupon_code').val(coupon_code);
                }


                $('.totall').text((parseFloat(subtotal) - discount_value) + ' VND');

            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    function showMessage(type, message) {
        setTimeout(function() {

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if (type == 'success') {
                toastr.success(message);
            } else {
                toastr.error(message);
            }
        }, 1300);
    }


    //Slide Categories

    $(".categories__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 4,
        dots: false,
        nav: true,
        navText: ["<span class='fas fa-angle-left'><span/>", "<span class='fas fa-angle-right'><span/>"],
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        responsive: {

            0: {
                items: 1,
            },

            480: {
                items: 2,
            },

            768: {
                items: 3,
            },

            992: {
                items: 4,
            }
        }
    });

    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function() {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });


})(jQuery);