<?php

use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\CouponController as AdminCouponController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\ProfileController as AdminProfileController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\GetLocalController;
use App\Http\Controllers\SubscriberController;
use App\Http\Controllers\User\HomepageController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\User\CartController;
use App\Http\Controllers\User\CheckoutController;
use App\Http\Controllers\User\OrderController as UserOrderController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::controller(HomepageController::class)->group(function () {

    Route::get('/', 'index')->name('homepage');
    Route::get('/about', 'about')->name('about');
    Route::get('/contact', 'contact')->name('contact');
    // Route::get('/blog', 'blog')->name('blog');
    Route::get('/chi-tiet-san-pham/{id}', 'showProduct')->name('home.showproduct');
});

Route::middleware(['auth'])->group(function () {

    Route::controller(HomepageController::class)->group(function () {
        Route::post('/binh-luan/{id}', 'storeComment')->name('home.storecomment');
        Route::get('/danh-sach-yeu-thich', 'showWishlist')->name('home.showwishlist');
        Route::post('/danh-sach-yeu-thich', 'addToWishlist')->name('wishlist.addproduct');
        Route::delete('/danh-sach-yeu-thich/{id}', 'deleteProduct')->name('wishlist.delete');
    });

    Route::controller(CartController::class)->group(function () {
        Route::post('/gio-hang', 'addToCart')->name('cart.addproduct');
        Route::get('/gio-hang', 'show')->name('cart.show');
        Route::put('/gio-hang/cap-nhat', 'update')->name('cart.update');
        Route::delete('/gio-hang/{id}', 'deleteProduct')->name('cart.delete');
        Route::get('/gio-hang/check-coupon', 'checkCoupon')->name('cart.check-coupon');
    });

    Route::controller(CheckoutController::class)->group(function () {
        Route::post('/chi-tiet-don-hang', 'order')->name('checkout.order');
        Route::get('feeship', 'feeShip')->name('feeship');
        Route::get('/thanh-toan', 'index')->name('checkout.index');
    });

    Route::controller(UserOrderController::class)->group(function () {
        Route::get('/quan-ly-don-hang', 'index')->name('home.order');
        Route::get('/chi-tiet-don-hang/{id}', 'show')->name('home.order.show');
    });


    Route::get('/guest/{id}/profile',[HomepageController::class,'showProfile'])->name('showProfile');

    // Admin

    Route::middleware('checklogin')->group(function () {
        Route::get('/home-admin', [HomeController::class, 'index'])->name('home.admin');
        Route::get('/notification', [HomeController::class, 'notification']);
        Route::resource('/category', AdminCategoryController::class)->except('create', 'show');
        Route::resource('/tag', TagController::class)->except('create', 'show');
        Route::resource('/product', AdminProductController::class)->except('create', 'show');
        Route::resource('/coupon', AdminCouponController::class)->except('create', 'show');
        Route::resource('/order', AdminOrderController::class);

        Route::resource('/role', RoleController::class)->except(['show']);
        Route::resource('/dashboard', AdminDashboardController::class);
        Route::get('/manage-user', [UserController::class, 'index'])->name('user.index');
        Route::put('/manage-user/{id}', [UserController::class, 'update'])->name('user.update');
        Route::get('/profile', [AdminProfileController::class, 'index'])->name('admin.profile');
        Route::put('/profile/{id}/update', [AdminProfileController::class, 'update'])->name('admin.updateProfile');

        Route::controller(AdminOrderController::class)->group(function () {
            Route::get('/order', 'index')->name('order.index');
            Route::post('/order/{id}', 'update')->name('order.update');
            Route::get('/order-export', 'orderExport')->name('order.export');
        });

        Route::controller(AdminDashboardController::class)->group(function () {
            Route::post('/filter-by-date', [AdminDashboardController::class, 'filterByDate'])->name('dashboard.filterByDate');
            Route::post('/dashboard-filter', [AdminDashboardController::class, 'dashboardFilter'])->name('dashboard.filter');
            Route::post('/day-order', [AdminDashboardController::class, 'dayOrder'])->name('dashboard.dayOrder');
        });

        Route::controller(PermissionController::class)->group(function () {
            Route::get('/permission/create', 'create')->name('permission.create');
            Route::post('/permission/store', 'store')->name('permission.store');
            Route::get('/get-childrenpermission', 'getChildrenPermission')->name('permission.getChildren');
        });
    });
});


Route::get('subscribe', [SubscriberController::class, 'sendMail'])->name('subscribe');

Route::get('district', [GetLocalController::class, 'district'])->name('district');
Route::get('village', [GetLocalController::class, 'village'])->name('village');

Route::controller(SocialController::class)->group(function () {

    Route::get('auth/google', 'redirectToGoogle')->name('loginGG');
    Route::get('auth/google/callback',  'handleGoogleCallback');


    Route::get('auth/facebook', 'redirectToFacebook')->name('loginFB');
    Route::get('auth/facebook/callback', 'handleFacebookCallback');
});

Route::get('/forgot-password', function () {
    return view('auth.passwords.email');
})->name('forgotPassword');


Route::get('get-password/{id}/{token}',[ForgotPasswordController::class, 'getPassword'])->name('forgotPassword.getPassword');
Route::post('set-password',[ForgotPasswordController::class, 'setPassword'])->name('forgotPassword.setPassword');