<?php

return [
    'access' => [
        'list_Category' => 'list_Category',
        'add_Category' => 'add_Category',
        'edit_Category' => 'edit_Category',
        'delete_Category' => 'delete_Category',
        //Product
        'list_Product' => 'list_Product',
        'edit_Product' => 'edit_Product',
        'add_Product' => 'add_Product',
        'delete_Product' => 'delete_Product',
        //Tag
        'list_Tag' => 'list_Tag',
        'edit_Tag' => 'edit_Tag',
        'delete_Tag' => 'delete_Tag',
        'add_Tag' => 'add_Tag',
        //Permission
        'add_Permission' => 'add_Permission',
        //Coupon
        'list_Coupon' => 'list_Coupon',
        'edit_Coupon' => 'edit_Coupon',
        'add_Coupon' => 'add_Coupon',
        'delete_Coupon' => 'delete_Coupon',
        //Order
        'list_Order' => 'list_Order',
        'edit_Order' => 'edit_Order',
        //Role
        'list_Role' => 'list_Role',
        'add_Role' => 'add_Role',
        'edit_Role' => 'edit_Role',
        'delete_Role' => 'delete_Role',
  
        //User

        'list_User' => 'list_User',
        'edit_User' => 'edit_User',

    ],

    'module_parent' => [
        'category',
        'slider',
        'product',
        'role',
        'permission',
        'user',
        'role',
    ],
    'module_children' => [
        'list',
        'add',
        'edit',
        'delete',
    ]
];
