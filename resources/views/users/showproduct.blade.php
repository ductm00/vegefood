@extends('layouts.user')
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url({{ asset('user/images/bg_1.jpg') }} )";>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('homepage')}}">Home</a></span> <span
                            class="mr-2"></span>
                    <h1 class="mb-0 bread">Product Details</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">
                <div class="col-lg-6 mb-5 ftco-animate">
                    <a href="{{ $product->image }}" class="image-popup"><img src="{{ $product->image }}"
                            class="img-fluid" alt="Vegefood"></a>
                </div>
                <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                    <h3 class="namproduct">{{ $product->name }}</h3>
                   
                    <div class="my-rating jq-stars"></div>

                    <div class="rating d-flex">
                        <p class="text-left mr-4">
                            <a href="#" class="mr-2" style="color: #000;">{{ $star_sum }} <span
                                    style="color: #bbb;">Đánh giá</span></a>
                        </p>
                        <p class="text-left">
                            <a href="#" class="mr-2" style="color: #000;">5 <span
                                    style="color: #bbb;">Lượt mua</span></a>
                        </p>
                    </div>
                    <p class="price">
                        <span>{{ $product->promotion_price ? $product->promotion_price : $product->price }} đ</span>
                    </p>
                    <p> {{ $product->description }}
                    </p>
                    <p><span>Số lượng trong kho: {{ $quantity_instock }}</span></p>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group d-flex">
                                Đơn vị : {{ $product->unit }}
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div class="input-group col-md-6 d-flex mb-3">
                            <span class="input-group-btn mr-2">
                                <button type="button" class="quantity-left-minus btn" data-type="minus" data-field="">
                                    <i class="ion-ios-remove"></i>
                                </button>
                            </span>
                            <input type="number" id="quantity" name="quantity" class="form-control input-number" value="1"
                                min="1" max="100">
                            <span class="input-group-btn ml-2">
                                <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                                    <i class="ion-ios-add"></i>
                                </button>
                            </span>
                        </div>
                        <div class="w-100"></div>

                    </div>
                    <p class="btn btn-black py-3 px-5 addmanytocart ">Add to Cart</p>

                    <div class="tagcloud06">
                        <ul>
                            @foreach ($tags as $tag)
                                <li><a
                                        href="{{ route('homepage', ['tag' => $tag->slug]) }}"><span>{{ $tag->name }}</span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <h2 class="mb-4">{{ __('Đánh giá sản phẩm') }}</h2>
                </div>
                <div class="col-lg-8 ftco-animate">
                    <div class="pt-5 mt-5">
                        <h3 class="mb-5">
                            {{ $star_sum }} {{ __('Đánh giá') }}
                        </h3>
                        <ul class="comment-list">
                            @foreach ($product->comments as $comment)
                                <li class="comment">
                                    <div class="vcard bio">
                                        <img src="{{ $comment->user->image }}" alt="Avatar">
                                    </div>
                                    <div class="comment-body">
                                        <h3>{{ $comment->user->name }}</h3>
                                        <div class="rated">
                                            <input type="radio" {{ $comment->rating == '5' ? 'checked' : '' }}
                                                class="rating-5">
                                            <label for="rating-5"></label>
                                            <input type="radio" {{ $comment->rating == '4' ? 'checked' : '' }}
                                                class="rating-4">
                                            <label for="rating-4"></label>
                                            <input type="radio" {{ $comment->rating == '3' ? 'checked' : '' }}
                                                class="rating-3">
                                            <label for="rating-3"></label>
                                            <input type="radio" {{ $comment->rating == '2' ? 'checked' : '' }}
                                                class="rating-2">
                                            <label for="rating-2"></label>
                                            <input type="radio" {{ $comment->rating == '1' ? 'checked' : '' }}
                                                class="rating-1">
                                            <label for="rating-1"></label>
                                        </div>
                                        <div class="meta">{{ $comment->created_at }}</div>
                                        <p>{{ $comment->content }}</p>
                                        <p><a href="#" class="reply">Reply</a></p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <!-- END comment-list -->

                        <div class="comment-form-wrap pt-5">
                            <h4 class="mb-5">{{ __('Để lại đánh giá của bạn') }}</h4>

                            <form action="{{ route('home.storecomment', [$product->id]) }}" method="POST"
                                class="p-5 bg-light" enctype="multipart/form-data">
                                @if (!Auth::check())
                                    {{ __('Bạn cần đăng nhập để đánh giá sản phẩm') }}
                                @endif

                                @csrf
                                <div class="form-group">
                                    <div class="rating">
                                        <input type="radio" name="rating" value="5" id="rating-5">
                                        <label for="rating-5"></label>
                                        <input type="radio" name="rating" value="4" id="rating-4">
                                        <label for="rating-4"></label>
                                        <input type="radio" name="rating" value="3" id="rating-3">
                                        <label for="rating-3"></label>
                                        <input type="radio" name="rating" value="2" id="rating-2">
                                        <label for="rating-2"></label>
                                        <input type="radio" name="rating" value="1" id="rating-1">
                                        <label for="rating-1"></label>

                                    </div>
                                    <label for="message">Message</label>

                                    <textarea name="content" id="message" cols="20" rows="5" class="form-control"></textarea>

                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Post Comment" class="btn btn-sm py-3 px-4 btn-primary ">
                                </div>

                            </form>
                        </div>
                    </div>
                </div> <!-- .col-md-8 -->

            </div>
        </div>
    </section> <!-- .section -->
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Products</span>
                    <h2 class="mb-4">Related Products</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach($relate_products as $relate_product)
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a href="{{ route('home.showproduct', [$relate_product->id]) }}" class="img-prod"><img class="img-fluid" src="{{$relate_product->image}}"
                                alt="Vegefood">
                                @if ($relate_product->promotion_price)
                                <span class="status">
                                    {{ round((1 - $relate_product->promotion_price / $relate_product->price) * 100) }}%</span>
                                @endif
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3><a href="{{ route('home.showproduct', [$relate_product->id]) }}">{{$relate_product->name}}</a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    @if ($relate_product->promotion_price)
                                <p class="price"><span class="mr-2 price-dc">{{ $relate_product->price }} VNĐ</span><span class="price-sale">{{ $relate_product->promotion_price }} VNĐ</span></p>
                                @else
                            <p class="price"><span>{{ $relate_product->price }} VNĐ</span></p>
                                @endif
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <button class="add-to-cart d-flex justify-content-center align-items-center text-center js-addcart-detail" style="color:#fff;background:#82ae46;width:40px;height:40px;margin: 0 auto; border-radius:50%;border-color:#82ae46">
                                        <span><i class="ion-ios-cart"></i></span>
                                        <input type="hidden" value="{{ $product->id }}" class="product_id">
    
                                    </button>
                                    <a href="{{ route('home.showproduct', [$product->id]) }}" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="ion-ios-eye"></i></span>
                                    </a>
    
                                    <button class="add-to-cart d-flex justify-content-center align-items-center text-center js-addcart-detail" style="color:#fff;background:#82ae46;width:40px;height:40px;margin: 0 auto; border-radius:50%;border-color:#82ae46">
                                        <span><i class="ion-ios-heart"></i></span>
                                        <input type="hidden" value="{{ $product->id }}" class="product_id">
    
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
    </section>

    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
                $(".my-rating").starRating({
                    initialRating: '{{$avg_star}}',
                    starSize: 20,
                    readOnly: true
                });

        });

        var quantity_instock = "<?php print $quantity_instock; ?>";
        var quantity = parseFloat($('.input-number').val());
        var _token = $('meta[name="csrf-token"]').attr('content');

        $('.quantity-right-plus').click(function() {
            if (quantity < quantity_instock) {
                quantity++;
            } else {
                alert('Trong kho chỉ còn ' + quantity_instock + ' sản phẩm');
                quantity = quantity_instock;
            }

            $('.input-number').val(quantity);
        })

        $('.quantity-left-minus').click(function() {
            if (quantity > 1) {
                quantity--;
            }

            $(this).parent().siblings('.input-number').val(quantity);
        })

        $('.input-number').on('change', function() {
            let val = $(this).val();
            if (parseFloat(val) > quantity_instock) {
                alert('Trong kho chỉ còn ' + quantity_instock + ' sản phẩm');
                quantity = quantity_instock;
                $(this).val(quantity_instock);
            } else {
                quantity = val;
                $(this).val(val);
            }
        })

        $('.addmanytocart').click(function() {
            let product_id = $('#product_id').val();
            let nameProduct = $('.namproduct').text();
            console.log(product_id);
            jQuery.ajax({
                url: '{{ route('cart.addproduct') }}',
                type: 'POST',
                data: {
                    product_id: product_id,
                    quantity: quantity,
                    '_token': _token,
                },
                success: function(response) {
                    if (response.status) {
                        $('.icon-shopping_cart').text('[' + response.quantity_in_cart + ']');

                        $('.addmanytocart').each(function() {
                            const ell = document.createElement('div');
                            ell.innerHTML =
                            "<a href='{{ route('cart.show') }}'>Giỏ hàng</a>";
                            swal({
                                title: nameProduct,
                                text: "Đã được thêm vào giỏ hàng !",
                                content: ell,
                                icon: "success",
                            });

                        });
                    } else {
                        $('.addmanytocart').each(function() {
                            swal({
                                title: nameProduct,
                                text: response.message,
                                icon: "warning",
                            });

                        });
                    }


                },
                error: function(error) {
                    $('.addmanytocart').each(function() {
                        const el = document.createElement('div');
                        el.innerHTML = "<a href='{{ route('login') }}'>Đăng nhập</a>";
                        swal({
                            text: "Bạn cần đăng nhập để có thể mua sản phẩm",
                            content: el,
                            icon: "warning",
                            dangerMode: true,
                        });
                    });
                }
            });
        })
    </script>
@endsection
