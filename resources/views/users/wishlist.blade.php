@extends('layouts.user')

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url({{ asset('user/images/bg_1.jpg') }});">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span>
                        <span>Wishlist</span></p>
                    <h1 class="mb-0 bread">My Wishlist</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-cart">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr class="text-center">
                                    <th>&nbsp;</th>
                                    <th>Hình ảnh</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (Auth::user()->likes as $product_in_wishlist)
                                    <tr class="text-center">
                                        <td class="product-remove"> <input type="hidden" class="wishlist_product_id"
                                                value="{{ $product_in_wishlist->id }}">
                                            <span class="ion-ios-close remove-wishlist"></span>
                                        </td>

                                        <td class="image-prod"> <a
                                                href="{{ route('home.showproduct', [$product_in_wishlist->id]) }}">
                                                <div class="img"
                                                    style="background-image:url({{ $product_in_wishlist->image }});"></div>
                                            </a></td>

                                        <td class="product-name">
                                            <h3>{{ $product_in_wishlist->name }}</h3>
                                            <p>{{ $product_in_wishlist->description }}</p>
                                        </td>

                                        <td class="price">{{ $product_in_wishlist->price }}</td>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Enter email address">
                            <input type="submit" value="Subscribe" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> --}}
@endsection

@section('script')
    <script>
        $('.remove-wishlist').on('click', function() {

            var wishlist_product_id = $(this).parent().find('.wishlist_product_id').val();
            var _token = $('meta[name="csrf-token"]').attr('content');

            jQuery.ajax({
                url: '/danh-sach-yeu-thich/' + wishlist_product_id,
                type: 'DELETE',
                data: {
                    id: wishlist_product_id,
                    '_token': _token,
                },
                success: function(response) {

                    $('.icon-wishlist').text('[' + response.quantity_in_wishlist + ']');
                },
                error: function(error) {
                    console.log(error);
                }
            });

            $(this).closest('.product-remove').parent().remove();


        });
    </script>
@endsection
