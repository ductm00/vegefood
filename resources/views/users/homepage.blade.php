@extends('layouts.user')
@section('content')
    <section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
            <div class="slider-item" style="background-image: url(user/images/bg_1.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

                        <div class="col-md-12 ftco-animate text-center">
                            <h1 class="mb-2">We serve Fresh Vegestables &amp; Fruits</h1>
                            <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
                            <p><a href="#" class="btn btn-primary">View Details</a></p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="slider-item" style="background-image: url(user/images/bg_2.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

                        <div class="col-sm-12 ftco-animate text-center">
                            <h1 class="mb-2">100% Fresh &amp; Organic Foods</h1>
                            <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
                            {{-- <p><a href="#" class="btn btn-primary">View Details</a></p> --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <section class="ftco-section ftco-no-pt ftco-no-pb py-5">
        <form class="d-flex form-group navbar-search " action="{{ route('homepage') }}" method="get">


            <div class="col-lg-2"></div>
            <div class="col-lg-2">
                <select name="category" class="form-control ">
                    <option value="">Category</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->slug }}"
                            {{ Request::get('category') == $category->slug ? 'selected' : '' }}>{{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <input type="text" class="form-control small " placeholder="Search anything..." aria-label="Search"
                name="search" value="{{ Request::get('search') }}" aria-describedby="basic-addon2">
            <div class="input-group-prepend col-lg-1 ">
                <button class="btn btn-primary" type="submit">
                    <i class="fas fa-search fa-sm "></i>
                </button>
            </div>
            <div class="col-lg-2"></div>


        </form>
    </section>
    <br>
    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">

                    @foreach ($categories as $category)
                        <div class="col-lg-3">
                            <div class="categories__item set-bg" data-setbg="{{ $category->image }}">
                                <h5><a
                                        href="{{ route('homepage', ['category' => $category->slug]) }}">{{ $category->name }}</a>
                                </h5>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <br>
    {{-- <section class="ftco-section ftco-category ftco-no-pt">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 order-md-last align-items-stretch d-flex">
                            <div class="category-wrap-2 ftco-animate img align-self-stretch d-flex"
                                style="background-image: url(user/images/category.jpg);">
                                <div class="text text-center">
                                    <h2>Vegetables</h2>
                                    <p>Protect the health of every home</p>
                                    <p><a href="#" class="btn btn-primary">Shop now</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                                style="background-image: url(user/images/category-1.jpg);">
                                <div class="text px-3 py-1">
                                    <h2 class="mb-0"><a href="#">Fruits</a></h2>
                                </div>
                            </div>
                            <div class="category-wrap ftco-animate img d-flex align-items-end"
                                style="background-image: url(user/images/category-2.jpg);">
                                <div class="text px-3 py-1">
                                    <h2 class="mb-0"><a href="#">Vegetables</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                        style="background-image: url(user/images/category-3.jpg);">
                        <div class="text px-3 py-1">
                            <h2 class="mb-0"><a href="#">Juices</a></h2>
                        </div>
                    </div>
                    <div class="category-wrap ftco-animate img d-flex align-items-end"
                        style="background-image: url(user/images/category-4.jpg);">
                        <div class="text px-3 py-1">
                            <h2 class="mb-0"><a href="#">Dried</a></h2>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section> --}}

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Sản phẩm nổi bật</span>
                    {{-- <h2 class="mb-4">Sản phẩm của chúng tôi</h2> --}}
                    {{-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p> --}}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <input type="hidden" class="user_id" value="{{ Auth::check() ? Auth::user()->id : '' }}">
                @foreach ($products as $product)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            <a href="{{ route('home.showproduct', [$product->id]) }}" class="img-prod"><img
                                    class="img-fluid" src="{{ $product->image }}" alt="Colorlib Template">
                                @if ($product->promotion_price)
                                    <span class="status">
                                        {{ round((1 - $product->promotion_price / $product->price) * 100) }}%</span>
                                @endif
                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="#" class="product_name">{{ $product->name }}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        @if ($product->promotion_price)
                                            <p class="price"><span class="mr-2 price-dc">{{ $product->price }}
                                                    VNĐ</span><span
                                                    class="price-sale">{{ $product->promotion_price }} VNĐ</span></p>
                                        @else
                                            <p class="price"><span>{{ $product->price }} VNĐ</span></p>
                                        @endif
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        <button
                                            class="add-to-cart d-flex justify-content-center align-items-center text-center js-addcart-detail"
                                            style="color:#fff;background:#82ae46;width:40px;height:40px;margin: 0 auto; border-radius:50%;border-color:#82ae46">
                                            <span><i class="ion-ios-cart"></i></span>
                                            <input type="hidden" value="{{ $product->id }}" class="product_id">

                                        </button>
                                        <a href="{{ route('home.showproduct', [$product->id]) }}"
                                            class="buy-now d-flex justify-content-center align-items-center mx-1">
                                            <span><i class="ion-ios-eye"></i></span>
                                        </a>

                                        <button
                                            class="add-to-wishlist d-flex justify-content-center align-items-center text-center js-addcart-detail"
                                            style="color:#fff;background:#82ae46;width:40px;height:40px;margin: 0 auto; border-radius:50%;border-color:#82ae46">
                                            <span><i class="ion-ios-heart"></i></span>
                                            <input type="hidden" value="{{ $product->id }}" class="product_id">

                                        </button>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div>
                {{ $products->appends(request()->query())->links('vendor.pagination.custom') }}
            </div>
        </div>
    </section>
    <hr>

    <section class="ftco-section">
        <div class="container">
            <div class="row no-gutters ftco-services">
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-shipped"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Ship toàn quốc</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-diet"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Luôn tươi ngon</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-award"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Chất lượng cao </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-customer-service"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Hỗ trợ 24/7</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {

            if ('{{ Session::has('success') }}' == 1) {
                showMessage('success', '{{ Session::get('success') }}');

            }

            function showMessage(type, message) {
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    if (type == 'success') {
                        toastr.success(message);
                    } else {
                        toastr.error(message);
                    }
                }, 1300);
            }



        })
        jQuery('.add-to-cart').click(function() {

            var user_id = $('.user_id').val();
            var product_id = $(this).children(".product_id").val();
            var _token = $('meta[name="csrf-token"]').attr('content');
            var nameProduct = $(this).parent().parent().parent().find(
                '.product_name').html();

            jQuery.ajax({
                url: '{{ route('cart.addproduct') }}',
                type: 'post',
                data: {
                    'user_id': user_id,
                    'product_id': product_id,
                    '_token': _token
                },
                success: function(response) {
                    if (response.status) {
                        $('.icon-shopping_cart').text('[' + response.quantity_in_cart + ']');

                        $('.js-addcart-detail').each(function() {
                            const e = document.createElement('div');
                            e.innerHTML = "<a href='{{ route('cart.show') }}'>Giỏ hàng</a>";
                            swal({
                                title: nameProduct,
                                text: "Đã được thêm vào giỏ hàng !",
                                content: e,

                                icon: "success",
                            });

                        });
                    } else {
                        $('.js-addcart-detail').each(function() {
                            swal({
                                title: nameProduct,
                                text: response.message,
                                icon: "warning",
                            });

                        });
                    }


                },
                error: function(error) {
                    $('.js-addcart-detail').each(function() {
                        const el = document.createElement('div');
                        el.innerHTML = "<a href='{{ route('login') }}'>Đăng nhập</a>";
                        swal({
                            text: "Bạn cần đăng nhập để có thể mua sản phẩm",
                            content: el,
                            icon: "warning",
                            dangerMode: true,
                        });
                    });
                }
            });
        });

        jQuery('.add-to-wishlist').click(function() {

            var user_id = $('.user_id').val();
            var product_id = $(this).children(".product_id").val();
            var _token = $('meta[name="csrf-token"]').attr('content');
            var nameProduct = $(this).parent().parent().parent().find(
                '.product_name').html();

            jQuery.ajax({
                url: '{{ route('wishlist.addproduct') }}',
                type: 'post',
                data: {
                    'user_id': user_id,
                    'product_id': product_id,
                    '_token': _token
                },
                success: function(response) {
                    if (response.status) {
                        $('.icon-wishlist').text('[' + response.quantity_in_wishlist + ']');

                        $('.js-addcart-detail').each(function() {
                            const e = document.createElement('div');
                            e.innerHTML = "<a href='{{ route('home.showwishlist') }}'>Danh sách yêu thích</a>";
                            swal({
                                title: nameProduct,
                                text: response.message,
                                content: e,

                                icon: "success",
                            });

                        });
                    } else {
                        $('.js-addcart-detail').each(function() {
                            swal({
                                title: nameProduct,
                                text: response.message,
                                icon: "warning",
                            });

                        });
                    }


                },
                error: function(error) {
                    $('.js-addcart-detail').each(function() {
                        const el = document.createElement('div');
                        el.innerHTML = "<a href='{{ route('login') }}'>Đăng nhập</a>";
                        swal({
                            text: "Bạn cần đăng nhập để có thể mua sản phẩm",
                            content: el,
                            icon: "warning",
                            dangerMode: true,
                        });
                    });
                }
            });
        });
    </script>
@endsection
