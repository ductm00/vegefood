@extends('layouts.user')

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('user/images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{ route('homepage') }}">Home</a></span>
                        <span>Cart</span>
                    </p>
                    <h1 class="mb-0 bread">My Cart</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-cart">
        <form action="{{ route('checkout.index') }}" method="GET">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ftco-animate">
                        <div class="cart-list">
                            <table class="table">
                                <thead class="thead-primary">
                                    <tr class="text-center">
                                        <th><input type="checkbox" id="checkall" /></th>
                                        <th>&nbsp;</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Giá</th>
                                        <th>Số lượng</th>
                                        <th>Tổng tiền</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($carts->products as $cart)
                                        @if($cart->pivot->quantity != '0')
                                        <tr class="text-center">
                                            <td class="check">
                                                <input type="checkbox" name="list[{{ $cart->pivot->product_id }}][id]"
                                                    value="{{ $cart->pivot->product_id }}" class="cb-element" />
                                                    
                                            </td>
                                            <td class="image-prod">
                                                <div class="img"
                                                    style="background-image:url({{ $cart->image }});">
                                                </div>
                                            </td>

                                            <td class="product-name">
                                                <h3>{{ $cart->name }}</h3>
                                                <p>{{ $cart->description }}</p>
                                            </td>

                                            <td class="price">{{ $cart->promotion_price ? $cart->promotion_price : $cart->price }} đ</td>

                                            <td class="shoping__cart__quantity">
                                                <div class="quantity">
                                                    <div class="pro-qty">
                                                        <input type="number" class="new_quantity"
                                                            value="{{ $cart->pivot->quantity }}" min="1" max="100">

                                                        <input type="hidden" class="cart_product_id"
                                                            value="{{ $cart->pivot->id }}">
                                                        <input type="hidden" class="quantity_instock"
                                                            value="{{ $cart->quantity_instock }}">

                                                        <input type="hidden"
                                                            name="list[{{ $cart->pivot->product_id }}][name]"
                                                            value="{{ $cart->name }}">
                                                        <input type="hidden"
                                                            name="list[{{ $cart->pivot->product_id }}][quantity]"
                                                            value="{{$cart->pivot->quantity}}" class="quantity_now">
                                                        <input type="hidden"
                                                            name="list[{{ $cart->pivot->product_id }}][price]"
                                                            class="product_price" value="{{ $cart->promotion_price ? $cart->promotion_price : $cart->price}}">
                                                    </div>
                                                </div>
                                            </td>

                                            <td class="total">
                                                @php
                                                    $sumProduct = ($cart->promotion_price ? $cart->promotion_price : $cart->price) * $cart->pivot->quantity;
                                                    echo $sumProduct;
                                                @endphp
                                            </td>
                                            <td class="product-remove">
                                                <input type="hidden" class="cart_product_id"
                                                    value="{{ $cart->pivot->id }}">
                                                <span class="ion-ios-close remove-cart "></span>
                                            </td>

                                        </tr><!-- END TR-->
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                        <div class="cart-total mb-3">
                            <h3>Mã giảm giá</h3>
                            <p>Nhập mã giảm giá nếu bạn có</p>
                            <div class="form-group">
                                <input type="text" class="form-control text-left px-3 coupon_code"
                                    style="text-transform:uppercase" placeholder="">
                                <input type="hidden" name="coupon_value" id="coupon_value">
                                <input type="hidden" name="coupon_code" id="coupon_code">
                                <div class="error-coupon">

                                </div>
                                <div class="success-coupon"></div> <br>
                                <button type="button" class="btn btn-primary py-3 px-4 check-coupon"
                                    >Kiểm tra</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate">

                    </div>
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                        <div class="cart-total mb-3">
                            <h3>Giỏ hàng</h3>
                            <p class="d-flex">
                                <span>Tổng tiền</span>
                                <span class="subtotal">0 VND</span>
                            </p>
                            <p class="d-flex">
                                <span>Giảm giá</span>
                                <span class="discount">0 VND</span>
                            </p>
                            <hr>
                            <p class="d-flex total-price">
                                <span>Tổng thanh toán</span>
                                <span class="totall">0 VND</span>
                            </p>
                        </div>
                        <p><button type="submit" class="btn btn-primary py-3 px-4" id="checkout">Thanh toán</button>
                        </p>
                    </div>
                </div>
            </div>
        </form>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        $(window).keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
        subtotal = 0;


        $('#checkall').change(function() {
            subtotal = 0;
            if ($(this).is(':checked')) {
                $('.cb-element').each(function() {
                    $(this).prop('checked', true).trigger('change');
                });
            } else {
                $('.cb-element').each(function() {
                    $(this).prop('checked', false).trigger('change');
                });
            }
        });

        $('.cb-element').change(function() {
        
            let item_price = parseFloat($(this).closest('tr').find('.total').text());
            subtotal = parseFloat($('.subtotal').text());
            if ($(this).is(':checked')) {
                subtotal += item_price

            } else {
                subtotal -= item_price;
            }

            $('.subtotal').text(subtotal + ' VND');

            var discount = $('.discount').text();

            $('.totall').text(parseFloat(subtotal) - parseFloat(discount) + 'VND');

            if ($('.cb-element:checked').length == $('.cb-element').length) {
                $('#checkall').prop('checked', true);
            } else {
                $('#checkall').prop('checked', false);
            }

            if ($(".cb-element:input:checkbox:checked").length != 0) {
                $('#checkout').prop('disabled', false);
            } else {
                $('#checkout').prop('disabled', true);
            }
        });

        $(document).ready(function() {
            if ($(".cb-element:input:checkbox:checked").length == 0) {
                $('#checkout').prop('disabled', true);
            }
        });
    </script>
@endsection
