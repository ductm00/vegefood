@extends('layouts.user')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('user/images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{ route('homepage') }}">Home</a></span>
                        <span>OrderDetail</span>
                    </p>
                    <h1 class="mb-0 bread">OrderDetail</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <form action="{{ route('checkout.order') }}" class="billing-form" method="POST">
               
                @csrf
                <div class="row justify-content-center">
                    <div class="col-xl-7 ftco-animate">

                        <h3 class="mb-4 billing-heading">Thông tin người nhận</h3>
                        <div class="row align-items-end">
                            <div class="col-md-6">
                                <div class="form-group">
                                    n
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                   l
                                </div>
                            </div>
                            <div class="w-100"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="emailaddress"></label>
                                    <input type="email" class="form-control @error('receiver_email') is-invalid @enderror"
                                        name="receiver_email" value="{{ old('receiver_email') }}"
                                        placeholder="Email Address">
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country">
                                      m
                                    </label>
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="receiver_city" id="city"
                                            class="form-control @error('receiver_phone') is-invalid @enderror">
                                            <option value="">Tỉnh/Thành phố</option>
                                           
                                                <option name="receiver_city" >
                                                   
                                                </option>
                                           
                                        </select>
                                       
                                            <span class="alert-danger" role="alert">
                                               k
                                            </span>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="streetaddress"></label>
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="receiver_district" id="district">
                                            <option value="">Quận/Huyện</option>
                                        </select>
                                    </div>
                                 
                                        <span class="alert-danger" role="alert">
                                          i
                                        </span>
                                  
                                </div>

                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="streetaddress"></label>
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="receiver_village" id="village">
                                            <option value="">Xã/Phường</option>
                                        </select>
                                    </div>
                                  
                                        <span class="alert-danger" role="alert">
                                           f
                                        </span>
                                  
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address"></label>
                                    d
                                </div>
                               
                                    <span class="alert-danger" role="alert">
                                       e
                                    </span>
                               
                            </div>

                        </div>

                        </div>
                    <div class="col-xl-5">
                        <div class="row mt-5 pt-3">
                            <div class="col-md-12 d-flex mb-5">
                                <div class="cart-detail cart-total p-3 p-md-4">
                                    <h3 class="billing-heading mb-4">Đơn hàng của bạn</h3>
                                    <p class="d-flex">
                                       <span><strong>Sản phẩm </strong></span>
                                       <span><strong>Giá</strong></span>
                                    </p>
                                   

                                            <p class="d-flex">
                                            <span><strong>sp1</strong></span>
                                            <span><strong>gia1</strong></span>
                                            </p>

                                            

                                          

                                    <p class="d-flex">
                                        <span><strong>Tạm tính</strong></span>
                                        
                                        
                                    </p>
                                    <p class="d-flex">
                                        <span><strong>Giảm giá</strong></span>
                                       
                                    </p>
                                    <p class="d-flex">
                                        <span><strong>Phí vận chuyển</strong></span>
                                       
                                    </p>

                                    <hr>
                                    <p class="d-flex total-price">
                                        <span><strong>Tổng tiền</strong></span>
                                        
                                    </p>
                                </div>
                            </div>
                            </div>
                    </div> <!-- .col-md-8 -->
                </div>
            </form><!-- END -->
        </div>
    </section> <!-- .section -->
@endsection