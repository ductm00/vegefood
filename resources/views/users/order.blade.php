@extends('layouts.user')
@section('content')

<div class="hero-wrap hero-bread" style="background-image: url('user/images/bg_1.jpg');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="{{ route('homepage') }}">Home</a></span>
                    <span>Orders</span>
                </p>
                <h1 class="mb-0 bread">My Orders</h1>
            </div>
        </div>
    </div>
</div>
{{-- <section class="ftco-section">

    <div class="container">
        <div class="row">

            <div class="col-lg-4 sidebar ftco-animate">
                <div class="sidebar-box">
                    <form action="#" class="search-form" style="display: flex">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search...">

                        </div>
                        <div class="form-group">
                            <select class="form-control" name="" id="">
                                <option value="">Trạng thái</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section> --}}

<section class="ftco-section">
    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">{{ __('Mã đơn hàng') }}</th>
                        <th scope="col">{{ __('Người nhận') }}</th>
                        <th scope="col">{{ __('Tổng tiền') }}</th>
                        <th scope="col">{{ __('Phương thức thanh toán') }}</th>
                        <th scope="col">{{ __('Trạng thái') }}
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->receiver_name }}</td>
                        <td>{{ $order->total }}</td>
                        <td>{{ $order->payment_method }}</td>

                        <td>{{ $order->status  }}
                        </td>
                        <td><button class='btn-huyx' data-toggle="modal" data-target=".bd-example-modal-lg{{$order->id}}" data-value="{{$order->id}}"><i class="far fa-eye"></i></button></td>
                    </tr>
                    <div class="modal fade bd-example-modal-lg{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><b>VEGEFOODS</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                
                                <div class="hero-wrap hero-breads" style="background-image: url('user/images/bg_1.jpg');">
                        <div class="container">
                            <div class="row no-gutters slider-text align-items-center justify-content-center">
                                <div class="col-md-9 ftco-animate text-center">
                                    <p class="breadcrumbs"><span class="mr-2"><a href="{{ route('homepage') }}">Home</a></span>
                                        <span>OrderDetail</span>
                                    </p>
                                    <h1 class="mb-0 bread">OrderDetail</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                                <div class="modal-body">
                                    <section class="ftco-section">
                                        <div class="container">
                                            <form action="{{ route('checkout.order') }}" class="billing-form" method="POST">
                
                                                @csrf
                                                <div class="justify-content-center row-order">
                                                    <div class="col-md-6 ftco-animate">
                
                                                        <h3 class="mb-4 billing-heading">Thông tin người nhận</h3>
                                                        <div class="row align-items-end">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Họ Tên: <span>{{$order->receiver_name}}</span> </label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>SĐT : <span>{{$order->receiver_phone}}</span> </label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Email : <span>{{$order->receiver_email}}</span> </label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Địa chỉ : <span>{{$order->receiver_address}}</span> </label>
                                                                </div>
                                                            </div>
                
                
                                                            <div class="w-100"></div>
                
                
                                                        </div>
                
                
                                                        <div class="row mt-5 pt-3">
                                                            <div class="col-md-12 d-flex mb-5">
                                                                <div class="cart-detail cart-total p-3 p-md-4">
                                                                    <h3 class="billing-heading mb-4">Đơn hàng của bạn</h3>
                
                                                                    <p class="d-flex">
                                                                        <span><strong>Sản phẩm </strong></span>
                                                                        <span><strong class="right">Giá</strong></span>
                                                                    </p>
                
                                                                    @foreach($order->products as $product)
                                                                    <p class="d-flex">
                                                                        <span><strong>{{$product->name}} x {{$product->pivot->quantity}}</strong></span> 
                                                                        <span><strong class="right">{{$product->pivot->price}}</strong></span>
                                                                    </p>

                                                                    @endforeach
                
                
                                                                    <hr>
                                                                    <p class="d-flex total-price">
                                                                        <span><strong>Tổng tiền: {{$order->total}}</strong></span>
                
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($order->status === 'Waiting')
                                                            <div>
                                                                <div class="form-group">
                                                                    <button class="btn-huy" data-value="{{$order->id}}" type="button">Hủy</button>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div> 
                                                </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                    

                    @endforeach

                </tbody>
            </table>

        </div>
    </div>


    <!-- Extra large modal -->
  
</section>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    
    $('#myModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })
</script>
<script>
     $('.btn-huy').click(function() {
        var order_id = $(this).data('value');
        var _token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
        title: 'Bạn chắc chắn?',
        text: "Đơn hàng sẽ bị hủy!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Xác nhận!',
        cancelButtonText: 'Không'
        }).then((result) => {
        if (result.isConfirmed) {
            jQuery.ajax({
                url: '/chi-tiet-don-hang/' + order_id,
                type: 'GET',
                data: {
                    order_id: order_id,
                    status:  'Cancel',
                    '_token': _token,
                },
                success: function (response) {

                    location.reload();
                },
                error: function (error) {
                    console.log(error);
                }
    });
        }
        })
    })
</script>
   
@endsection