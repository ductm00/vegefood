@extends('layouts.user')

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('user/images/bg_1.jpg');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span>
                        <span>Checkout</span>
                    </p>
                    <h1 class="mb-0 bread">Checkout</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <form action="{{ route('checkout.order') }}" class="billing-form" method="POST">
                @method('POST')
                @csrf
                <input type="hidden" class="city_name" name="city_name" value="">
                <input type="hidden" class="district_name" name="district_name" value="">
                <input type="hidden" class="village_name" name="village_name" value="">
                <div class="row justify-content-center">
                    <div class="col-xl-7 ftco-animate">

                        <h3 class="mb-4 billing-heading">Thông tin người nhận</h3>
                        <div class="row align-items-end">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control @error('receiver_name') is-invalid @enderror"
                                        name="receiver_name" value="{{ old('receiver_name') }}" placeholder="Họ và tên">
                                    @error('receiver_name')
                                        <span class="alert-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control @error('receiver_phone') is-invalid @enderror"
                                        name="receiver_phone" value="{{ old('receiver_phone') }}"
                                        placeholder="Số điện thoại">
                                    @error('receiver_phone')
                                        <span class="alert-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-100"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="emailaddress"></label>
                                    <input type="email" class="form-control @error('receiver_email') is-invalid @enderror"
                                        name="receiver_email" value="{{ old('receiver_email') }}"
                                        placeholder="Email Address">
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country">
                                        @error('receiver_email')
                                            <span class="alert-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </label>
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="receiver_city" id="city"
                                            class="form-control @error('receiver_phone') is-invalid @enderror">
                                            <option value="">Tỉnh/Thành phố</option>
                                            @foreach ($cities as $value)
                                                <option name="receiver_city" value="{{ $value['ProvinceID'] }}">
                                                    {{ $value['ProvinceName'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('receiver_city')
                                            <span class="alert-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="streetaddress"></label>
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="receiver_district" id="district"
                                            class="form-control @error('receiver_district') is-invalid @enderror">
                                            <option value="">Quận/Huyện</option>
                                        </select>
                                    </div>
                                    @error('receiver_district')
                                        <span class="alert-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="streetaddress"></label>
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="receiver_village" id="village"
                                            class="form-control @error('receiver_village') is-invalid @enderror">
                                            <option value="">Xã/Phường</option>
                                        </select>
                                    </div>
                                    @error('receiver_village')
                                        <span class="alert-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address"></label>
                                    <input type="text" name="receiver_address"
                                        class="form-control @error('receiver_address') is-invalid @enderror"
                                        value="{{ old('receiver_address') }}" placeholder="Address">
                                </div>
                                @error('receiver_address')
                                    <span class="alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="col-xl-5">
                        <div class="row mt-5 pt-3">
                            <div class="col-md-12 d-flex mb-5">
                                <div class="cart-detail cart-total p-3 p-md-4">
                                    <h3 class="billing-heading mb-4">Đơn hàng của bạn</h3>
                                    <p class="d-flex">
                                        <span><strong>Sản phẩm</strong></span>
                                        <span><strong>Giá</strong></span>
                                    </p>
                                    @php $total = 0 @endphp
                                    @foreach ($list_products as $product)
                                        @if (isset($product['id']))
                                            <p class="d-flex">
                                                <span>{{ $product['name'] }} x{{ $product['quantity'] }} </span>
                                                <span>{{ $product['price'] }}</span>
                                            </p>

                                            <input type="hidden" name="list[{{ $product['id'] }}][id]"
                                                value="{{ $product['id'] }}">
                                                <input type="hidden" name="list[{{ $product['id'] }}][price]"
                                                value="{{ $product['price'] }}">
                                            <input type="hidden" name="list[{{ $product['id'] }}][quantity]"
                                                value="{{ $product['quantity'] }}">

                                            @php
                                                $sumProduct = $product['quantity'] * $product['price'];
                                                // echo $sumProduct;
                                                
                                                $total += $sumProduct;
                                            @endphp
                                        @endif
                                    @endforeach

                                    <p class="d-flex">
                                        <span><strong>Tạm tính</strong></span>
                                        <span><strong>{{ $total }}</strong></span>
                                        <input type="hidden" id="subtotal" value="{{ $total }}">
                                    </p>
                                    <p class="d-flex">
                                        <span><strong>Giảm giá</strong></span>
                                        <span class="discount">{{ $coupon_value * $total }}</span>
                                    </p>
                                    <p class="d-flex">
                                        <span><strong>Phí vận chuyển</strong></span>
                                        <span class="feeship"></span>
                                    </p>

                                    <hr>
                                    <p class="d-flex total-price">
                                        <span><strong>Tổng tiền</strong></span>
                                        <input type="hidden" id="total" name="total">
                                        <span class="total"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="cart-detail p-3 p-md-4">
                                    <h3 class="billing-heading mb-4">Phương thức thanh toán</h3>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label><input type="radio" name="payment_method" value="cod"
                                                        class="mr-2">
                                                    Thanh toán khi nhận hàng</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label><input type="radio" name="payment_method" value="vnpay"
                                                        class="mr-2">
                                                    VNPAY
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="radio">
                                                <label><input type="radio" name="payment_method" value="paypal"
                                                        class="mr-2">
                                                    Paypal</label>
                                            </div>
                                        </div>
                                    </div> --}}

                                    @error('payment_method')
                                    <span class="alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    {{-- <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="" class="mr-2"> I have read
                                                    and
                                                    accept the terms and conditions</label>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <input type="hidden" name="coupon_code" value="{{ $coupon_code }}" id="">
                                    <p><button type="submit" class="btn btn-primary py-3 px-4 btn-order">Thanh toán</button></p>


                                </div>
                            </div>

                        </div>
                    </div> <!-- .col-md-8 -->
                </div>
            </form><!-- END -->
        </div>
    </section> <!-- .section -->

    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                    <span>Get e-mail updates about our latest shops and special offers</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#city").select2({
            placeholder: "Chọn Tỉnh/Thành phố",
            allowClear: true
        });
        $("#district").select2({
            placeholder: "Chọn Quận/Huyện",
            allowClear: true
        });
        $("#village").select2({
            placeholder: "Chọn Xã/Phường",
            allowClear: true
        });

        $(document).ready(function() {
            $('#city').on('change', function(e) {
                $('.city_name').val($(this).siblings('.select2-container').find('.select2-selection__rendered').text());
                var ProvinceID = e.target.value;
                $.ajax({
                    url: "{{ route('district') }}",
                    type: "GET",
                    data: {
                        ProvinceID: ProvinceID
                    },
                    success: function(data) {
                        $('.abc').remove();
                        $('.xyz').remove();
                        $.each(data.districts, function(index,
                            district) {
                            $('#district').append('<option class="abc" value="' +
                                district
                                .DistrictID + '">' + district.DistrictName +
                                '</option>');
                        })
                    }
                })
            });

            $('#district').on('change', function(e) {
                $('.district_name').val($(this).siblings('.select2-container').find('.select2-selection__rendered').text());

                var district_id = e.target.value;
                $.ajax({
                    url: "{{ route('village') }}",
                    type: "GET",
                    data: {
                        district_id: district_id
                    },
                    success: function(data) {
                        $('.xyz').remove();
                        $.each(data.villages, function(index,
                            village) {
                            $('#village').append('<option class="xyz" value="' + village
                                .WardCode + '">' + village.WardName + '</option>');
                        })
                    }
                })
            });

            $('#village').on('change', function(e) {
                $('.village_name').val($(this).siblings('.select2-container').find('.select2-selection__rendered').text());


                var insurance_value = parseFloat($('#subtotal').val());
                var service_type_id = '2';
                var coupon = '';
                var to_ward_code = e.target.value;
                var to_district_id = $('#district').find(":selected").val();
                var from_district_id = '1485';
                var weight = '1000';
                var length = '15';
                var width = '15';
                var height = '15';
                $.ajax({
                    url: "{{ route('feeship') }}",
                    type: "GET",
                    data: {
                        insurance_value: insurance_value,
                        service_type_id: service_type_id,
                        coupon: coupon,
                        to_ward_code: to_ward_code,
                        to_district_id: to_district_id,
                        from_district_id: from_district_id,
                        weight: weight,
                        length: length,
                        width: width,
                        height: height,
                    },
                    success: function(data) {

                        $('.feeship').text(data.feeship.total);

                        // var feeship = $('.feeship').text() == '' ? '0' : $('.feeship').text();
                        // console.log(feeship);
                        var total = parseFloat($('#subtotal').val()) - parseFloat($('.discount')
                            .text()) + parseFloat(data.feeship.total);

                        $('.total').text(total);
                        $('#total').val(total);
                    }
                })
            });

            // var feeship = $('.feeship').text() == '' ? '0' : $('.feeship').text();
            // console.log(feeship);
            // var total = parseFloat($('#subtotal').val()) - parseFloat($('.discount').text()) + parseFloat(feeship);

            // $('.total').text(total);
        });
    </script>
@endsection
