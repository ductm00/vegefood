<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home.admin') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="far fa-lemon"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Vegefoods <sup>r</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{request()->segment(1) == 'dashboard' ? 'active' : ''}}">
        <a class="nav-link" href="{{ route('dashboard.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->

    <!-- Nav Item - Pages Collapse Menu -->
    @can('category-list')
    <li class="nav-item {{request()->segment(1) == 'category' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('category.index') }}">
            <i class="fas fa-cubes"></i>
            <span>Categories</span>
        </a>
    </li>
    @endcan
    @can('tag-list')

    <li class="nav-item {{request()->segment(1) == 'tag' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('tag.index') }}">
            <i class="fas fa-tag"></i>
            <span>Tags</span>
        </a>
    </li>
    @endcan
    @can('product-list')

    <li class="nav-item {{request()->segment(1) == 'product' ? 'active' : ''}} ">
        <a class="nav-link collapsed" href="{{ route('product.index') }}">
            <i class="fas fa-cube"></i>
            <span>Products</span>
        </a>
    </li>
    @endcan
    @can('coupon-list')
    <li class="nav-item {{request()->segment(1) == 'coupon' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('coupon.index') }}">
            <i class="fas fa-ticket-alt"></i>
            <span>Coupons</span>
        </a>
    </li>
    @endcan
    @can('order-list')

    <li class="nav-item {{request()->segment(1) == 'order' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('order.index') }}">
            <i class="fas fa-shopping-cart"></i>
            <span>Orders</span>
        </a>
    </li>

    @endcan

    @can('permission-add')

    <li class="nav-item {{request()->segment(1) == 'permission' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('permission.create') }}">
            <i class="fas fa-user-lock"></i>
            <span>{{ __('Permissions')}}</span>
        </a>
    </li>

    @endcan

    @can('role-list')

    <li class="nav-item {{request()->segment(1) == 'role' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('role.index') }}">
            <i class="fas fa-user-shield"></i>
            <span>{{ __('Roles')}}</span>
        </a>
    </li>
    @endcan

    @can('user-list')

    <li class="nav-item {{request()->segment(1) == 'manage-user' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('user.index') }}">
            <i class="fas fa-user"></i>
            <span>Users</span>
        </a>
    </li>

    @endcan

    <!-- Nav Item - Utilities Collapse Menu -->
    {{-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-cube"></i>
            <span>Products</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Products:</h6>
                <a class="collapse-item" href="{{ route('product.index') }}">Products List</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#coupons" aria-expanded="true"
            aria-controls="coupons">
            <i class="fas fa-ticket-alt"></i>
            <span>Coupons</span>
        </a>
        <div id="coupons" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Coupons:</h6>
                <a class="collapse-item" href="{{ route('coupon.index') }}">Coupons List</a>
            </div>
        </div>
    </li> --}}
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
