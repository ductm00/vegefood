@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Coupon {{ $coupon->code }} Edit</h6>
            </div>

            <form method="post" class="form-horizontal" action="{{ route('coupon.update', [$coupon->code]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                {{-- <input type="hidden" name="id" value="{{ $student->id }}"> --}}
                <div style="text-align: center ; margin-top: 30px">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Code <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="code" name="code" value="{{ old('code', $coupon->code) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('code')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Value <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="value" name="value" value="{{ old('value', $coupon->value) }}">
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('value')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Start Date <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="date" class="form-control"
                                id="start_date" name="start_date" value="{{ old('start_date', $coupon->start_date) }}">
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('start_date')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">End Date <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="date" class="form-control"
                                id="end_date" name="end_date" value="{{ old('end_date', $coupon->end_date) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('end_date')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div style="margin-left : 900px" class="col-lg-21">
                            <button class="btn btn-primary" type="submit" name="update">Update</button>
                            <a class="btn btn-info " href="{{ route('category.index') }}">Back</a>
                        </div>
                    </div>
            </form>

        </div>
    </div>
    </div>
@endsection
