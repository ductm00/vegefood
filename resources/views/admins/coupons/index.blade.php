@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                {{-- <h6 class="m-0 font-weight-bold text-primary">Home</h6> --}}
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('coupon.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Coupons List</a>
                {{-- <h6 style="display: flex" class="m-0 font-weight-bold text-primary">Categories List</h6> --}}

            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                    action="{{ route('coupon.index') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" id="search" name="search" value="{{ Request::get('search') }}"
                            aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">\

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <div style="margin: 14px 0px 0px 0px" class="col-sm-2">
                        <div class="form-group">
                            <div style="display: flex;" class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-primary" href="" data-toggle="modal" data-target=".bd-example-modal-lg"><i
                                        class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </ul>



            </nav>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Value</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($coupons != null)
                                @foreach ($coupons as $key => $coupon)
                                    <tr>
                                        <td>{{ $coupon->code }}</td>
                                        <td>{{ $coupon->value }}</td>
                                        <td>{{ $coupon->start_date }}</td>
                                        <td>{{ $coupon->end_date }}</td>
                                        <td>
                                            <div style="display: flex">
                                                <button style=" width: 35px; height: 35px;" type="button"
                                                    class="btn btn-primary btn-sm"><a style="color: white ;"
                                                        href="{{ route('coupon.edit', [$coupon->code]) }}"
                                                        class="ac-button">
                                                        <span class="far fa-edit"></span></a></button>

                                                <form action="{{ route('coupon.destroy', [$coupon->code]) }}"
                                                    method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button style=" width: 35px; height: 35px; margin-left: 5px"
                                                        type="submit" class="btn btn-danger btn-sm"
                                                        onclick="return confirm('Are you sure you want to delete this coupon ?')">
                                                        <span class="far fa-trash-alt"></span></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $coupons->appends(request()->query())->links('vendor.pagination.custom') }}
        </div>
    </div>


    <!-- Extra large modal -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">



                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" id="form" action="{{ route('coupon.store') }}">
                            @csrf
                            <div style="text-align: center">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Code <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="code" name="code" value="{{ old('code') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('code')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror

                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Value <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="value" name="value" value="{{ old('value') }}">
                                        <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('value')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror

                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Start Date <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="date"
                                            class="form-control" id="start_date" name="start_date"
                                            value="{{ old('start_date') }}"> <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('start_date')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror

                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">End Date <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="date"
                                            class="form-control" id="end_date" name="end_date"
                                            value="{{ old('end_date') }}"> <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('end_date')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="submit" id="submit" name="submit">Create</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                            </div>
                        </form>
                        <div id="message"></div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <script>
        $('#myModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('#search').on('keyup', function() {
            $value = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ URL::to('coupon') }}',
                data: {
                    'search': $value
                },
                success: function(data) {
                    $('tbody').html(data);
                }
            });
        })
        $.ajaxSetup({
            headers: {
                'csrftoken': '{{ csrf_token() }}'
            }
        });
    </script>
@endsection
