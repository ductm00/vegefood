@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home.admin') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('role.index') }}">{{ __('List Roles') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Edit Role ') }} {{ $role->name }}</li>
            </ol>
        </nav>
        <div class="card shadow mb-4">


            <div class="card-body">

                <div id="content">
                    <form method="POST" class="form-horizontal" action="{{ route('role.update', $role->id) }}">
                        @method('PUT')
                        @csrf
                        <div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-12 control-label">{{ __('Name') }} <i
                                        class="note"><span class="text-danger">*</span></i></label>
                                <div class="col-sm-12"><input type="text" class="form-control" name="name"
                                        value="{{ old('name', $role->name) }}"> <span class="help-block m-b-none"></span>
                                </div>
                            </div>
                            @error('name')
                                <span class="errors text-danger">{{ $message }}</span>
                            @enderror
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-12 control-label">{{ __('Display_name') }}
                                    <i class="note"><span class="text-danger">*</span></i></label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control"
                                        name="display_name"> {{ old('display_name', $role->display_name) }}</textarea> <span class="help-block m-b-none"></span>
                                </div>
                            </div>
                            @error('display_name')
                                <span class="errors text-danger">{{ $message }}</span>
                            @enderror

                            <div class="form-group">
                                <h5 class="card-title col-md-3">
                                    <label>

                                        <input type="checkbox" class="form-group checkall" value="">
                                    </label>
                                    {{ __('Check all') }}
                                </h5>
                            </div>

                            <div class="form-group">
                                <h5 class="card-title col-md-3">
                                    <label>

                                        <input type="checkbox" class="form-group" name="permission_id[]" value="1"
                                            {{ $role->permissions->contains('1') ? 'checked' : '' }}>
                                    </label>
                                    {{ __('Login Admin') }}
                                </h5>
                            </div>

                            @foreach ($parentPermissions as $parentPermission)
                                <div class="hr-line-dashed"></div>

                                <div class="col-md-12 form-group">

                                    <div class="card">
                                        <div class="card-header">
                                            <label>
                                                <input type="checkbox" class="parentPermission form-group" value="">
                                            </label>
                                            {{ $parentPermission->name }}
                                        </div>

                                        <div class="card-body">
                                            <div class="row">

                                                @foreach ($parentPermission->childrentPermissions as $childrentPermission)
                                                    <h5 class="card-title col-md-3">
                                                        <label>
                                                            <input type="checkbox" name="permission_id[]"
                                                                class="form-group childrentPermission "
                                                                value="{{ $childrentPermission->id }}"
                                                                {{ $role->permissions->contains($childrentPermission->id) ? 'checked' : '' }}>
                                                        </label>
                                                        {{ $childrentPermission->name }}
                                                    </h5>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            @endforeach
                            <div class="col-md-12 form-group">
                                <a href="{{ route('role.index') }}" class="btn btn-primary">{{ __('Back') }}</a>
                                <button class="btn btn-primary" type="submit">{{ __('Update') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            if ($('.parentPermission').closest('.card').find('.childrentPermission:checked').length == $(
                    '.parentPermission').closest('.card').find('.childrentPermission').length) {
                $('.checkall').prop('checked', true);
            } else {
                $('.checkall').prop('checked', false);
            }

            $('.parentPermission').each(function(index, val){
               if( $(this).closest('.card').find('.childrentPermission').length == $(this).closest('.card').find('.childrentPermission:checked').length) {
                   $(this).prop('checked',true);
               }
            })

            $('.checkall').change(function() {
                if ($(this).is(':checked')) {

                    $('.childrentPermission').prop('checked', true);
                    $('.parentPermission').prop('checked', true);
                } else {
                    $('.childrentPermission').prop('checked', false);
                    $('.parentPermission').prop('checked', false);
                }
            })



            $('.parentPermission').change(function() {
                if ($(this).is(':checked')) {
                    $(this).closest('.card').find('.childrentPermission').prop('checked', true);
                } else {

                    $(this).closest('.card').find('.childrentPermission').prop('checked', false);
                }
            })

            $('.childrentPermission').change(function() {
                if ($(this).closest('.row').find('.childrentPermission:checked').length == $(this).closest(
                        '.row').find('.childrentPermission').length) {
                    $(this).closest('.card').find('.parentPermission').prop('checked', true);
                } else {
                    $(this).closest('.card').find('.parentPermission').prop('checked', false);
                }
            })

        })
    </script>
@endsection
