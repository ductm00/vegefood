@extends('layouts.admins.main')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('role.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Roles List</a>

            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                    action="{{ route('role.index') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" id="search" name="search" value="{{ Request::get('search') }}"
                            aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                   
                    <div style="margin: 14px 0px 0px 0px" class="col-sm-2">
                        <div class="form-group">
                            <div style="display: flex;" class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-primary" href="{{ route('role.create') }}"><i
                                        class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </ul>
            </nav>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Display_name') }}</th>
                                <th>{{ __('Permission') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($roles)
                                @foreach ($roles as $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->display_name }}</td>
                                        <td>
                                            @foreach ($role->permissions as $permission)
                                            <span class="badge badge-secondary"> {{ $permission->name }} </span>
                                            @endforeach
                                        </td>
                                        <td>
                                            <div style="display: flex">
                                                <button style=" width: 35px; height: 35px;" type="button"
                                                    class="btn btn-primary btn-sm"><a style="color: white ;"
                                                        href="{{ route('role.edit', [$role->id]) }}"
                                                        class="ac-button">
                                                        <span class="far fa-edit"></span></a></button>

                                                <form action="{{ route('role.destroy', [$role->id]) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button style=" width: 35px; height: 35px; margin-left: 5px"
                                                        type="submit" class="btn btn-danger btn-sm"
                                                        onclick="return confirm('Are you sure you want to delete this role ?')">
                                                        <span class="far fa-trash-alt"></span></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            @if ($roles)
                {{ $roles->appends(request()->query())->links('vendor.pagination.custom') }}
            @endif
        </div>
    </div>
@endsection