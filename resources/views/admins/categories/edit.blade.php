@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Category {{ $category->name }} Edit</h6>
            </div>

            <form method="post" class="form-horizontal" action="{{ route('category.update', [$category->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                {{-- <input type="hidden" name="id" value="{{ $student->id }}"> --}}
                <div style="text-align: center ; margin-top: 30px">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Name <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-7"><input type="text" class="form-control name"
                        name="name" onkeyup="ChangeToSlug(this)" value="{{ old('name', $category->name) }}" > <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('name')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Slug <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-7"><input type="text"
                                class="form-control convert_slug" name="slug" value="{{ old('slug', $category->slug) }}" > <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('slug')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label style="margin-left: -422px" class="col-sm-2 control-label">Image <i
                                class="note text-danger">*</i></label>
                        <label style="margin-left: 15px;" class="image"><input type="file" id="image" name="image"
                                aria-label="File browser example">
                            <span class="file-custom"></span>
                        </label>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div style="margin-left : 700px" class="col-lg-21">
                            <button class="btn btn-primary" type="submit" name="update">Update</button>
                            <a class="btn btn-info " href="{{ route('category.index') }}">Back</a>
                        </div>
                    </div>
            </form>

        </div>
    </div>
    </div>

















    {{-- <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Categories List</h6>
            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                            aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                        placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <div style="margin: 14px 0px 0px 0px" class="col-sm-2">
                        <div class="form-group">
                            <div style="display: flex;" class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-primary" href="" data-toggle="modal" data-target=".bd-example-modal-lg"><i
                                        class="fa fa-plus"></i></a>
                                <a title="Export Data" style="margin-left: 5px;" class="btn btn-info" href=""><i
                                        class="fas fa-share-square"></i></a>
                            </div>
                        </div>
                    </div>
                </ul>



            </nav>




            
        </div>
    </div> --}}
@endsection
