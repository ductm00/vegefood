@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home.admin') }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Create Permissions') }}</li>
            </ol>
        </nav>
        <div class="card shadow mb-4">


            <div class="card-body">
                <div id="content">
                    <form method="post" class="form-horizontal" id="form" action="{{ route('permission.store') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div>
                          
                            <div class="hr-line-dashed"></div>
                            <div class="col-md-12 form-group">

                                <div class="card">
                                    <div class="card-header">
                                        <label class="col-sm-12 control-label"> {{ __('Module parent') }} <i
                                                class="text-danger">*</i></label>

                                        <div class="col-sm-12">
                                            <select class="form-control moduleParent" name="moduleParent">
                                                <option value="">Module parent</option>
                                                @foreach ($parentPermission as $moduleParent)
                                                    <option value="{{ $moduleParent->id }}">{{ $moduleParent->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <span class="help-block m-b-none"></span>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="row">

                                            @foreach (config('permissions')['module_children'] as $moduleChildren)
                                                <h5 class="card-title col-md-3">
                                                    <label>
                                                        <input type="checkbox" name="moduleChildren[]"
                                                            class="form-group moduleChildren "
                                                            value="{{ $moduleChildren }}">
                                                    </label>
                                                    {{ $moduleChildren }}
                                                </h5>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="col-md-12 form-group">
                                <button class="btn btn-primary" type="submit">{{ __('Create') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.moduleParent').change(function() {
            var permission_id = $(this).val();
            var _token = $('meta[name="csrf-token"]').attr('content');

            jQuery.ajax({
                url: "{{ route('permission.getChildren') }}",
                type: 'GET',
                data: {
                    id: permission_id,
                    '_token': _token,
                },
                success: function(respone) {
                    $('.moduleChildren').each(function() {
                        if(jQuery.inArray($(this).val(), respone.childrenPermissions) != -1) {
                            $(this).prop('checked', true);
                        } else {
                            $(this).prop('checked', false);
                        }
                    })
                },

            });
        })
    </script>
@endsection
