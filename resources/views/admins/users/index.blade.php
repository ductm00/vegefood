@extends('layouts.admins.main')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('user.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Users List</a>

            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                    action="{{ route('user.index') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" name="search" value="{{ Request::get('search') }}"
                            aria-describedby="basic-addon2">

                            <div style="margin: 0px 10px 0px 10px" class="">
                                <select name="role_id" class="form-control ">
                                    <option>Select Role</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" {{Request::get('role_id') == $role->id ? 'selected' : ''}}>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
            </nav>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Phone') }}</th>
                                <th>{{ __('Role') }}</th>
                                <th>{{__('Action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($users)
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>
                                        @if ($user->roles)
                                        <td>
                                            @foreach ($user->roles as $roleUser)
                                                <span class="badge badge-secondary">{{ $roleUser->name }}</span>
                                            @endforeach
                                        </td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>
                                            <div style="display: flex">
                                                <button style=" width: 35px; height: 35px;" type="button"
                                                    class="btn btn-info btn-sm" data-toggle="modal"
                                                    data-target="#detailModal{{ $user->id }}">
                                                    <span class="far fa-edit"></span></button>
                                                <div class="modal fade" id="detailModal{{ $user->id }}"
                                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-xl" role="document">
                                                        <div class="modal-content">
                                                            <form action="{{ route('user.update', [$user->id]) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('PUT')
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                                        {{ __('Edit User') }} {{ $user->name }} </h5>

                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="ibox-content">
                                                                        <div class="form-group"><label
                                                                                class="col-sm-2 control-label">
                                                                                {{ __('Roles') }} <i
                                                                                    class="note"></i></label>
                                                                            <div style="display: inline-block"
                                                                                class="col-sm-9">
                                                                                <select class="role_id form-control"
                                                                                    name="role_id[]" multiple="multiple">
                                                                                    @foreach ($roles as $role)
                                                                                        <option
                                                                                            value="{{ $role->id }}" {{$user->roles->contains($role->id) ? 'selected' : ''}}>
                                                                                            {{ $role->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <span class="help-block m-b-none"></span>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Save
                                                                        changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            @if ($users)
                {{ $users->appends(request()->query())->links('vendor.pagination.custom') }}
            @endif
        </div>
    </div>


@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.role_id').select2();
        });
        $('#myModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })
    </script>
@endsection
