@extends('layouts.admins.main')
@section('content')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                <a style="text-decoration: none" href="" class="">Home /</a>
                <a href="" style="margin-left:5px; text-decoration: none" class="font-weight-bold text-primary">Dashboard</a>
            </div>
            <br>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div> 
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">Best Selling Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">Worst Selling Products</a>
                        </li>
                    </ul>
                    <div class="col-md-8">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Product Name</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Quantity</p>
                                    </div>
                                </div>
                                <div class="row">

                                    @foreach ($best_selling_products as $best_selling_product)
                                        <div class="col-md-6">
                                            <label>{{ $best_selling_product->product->name }}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{ $best_selling_product->quantity }}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Product Name</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Quantity</p>
                                    </div>
                                </div>
                                <div class="row">

                                    @foreach ($worst_selling_products as $worst_selling_product)
                                        <div class="col-md-6">
                                            <label>{{ $worst_selling_product->product->name }}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{ $worst_selling_product->quantity }}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="ibox-title">
                    <h5>Revenue Statistical
                    </h5>
                </div>
                <div class="row">
                    <form style="display: flex " class="col-lg-11" action="" method="post">
                        @csrf
                        <div class="col-lg-4">
                            <label for="">From Date</label>
                            <input type="text" id="fromdate" class="datepicker" autocomplete="off" readonly="true">
                        </div>
                        <div class="col-lg-4">
                            <label for="">To Date</label>
                            <input type="text" id="todate" class="datepicker" autocomplete="off" readonly="true">
                        </div>
                        <div style="" class="col-lg-2">
                            <select name="datefilter" id="datefilter" class="form-control ">
                                <option selected disabled>Select filter</option>
                                <option id="thisweek" value="thisweek"
                                    {{ Request::get('datefilter') == 'thisweek' ? 'selected' : '' }}> This Week
                                </option>
                                <option id='thismonth' value="thismonth"
                                    {{ Request::get('datefilter') == 'thismonth' ? 'selected' : '' }}> This Month
                                </option>
                                <option id='thisyear' value="thisyear"
                                    {{ Request::get('datefilter') == 'thisyear' ? 'selected' : '' }}> This Year</option>
                            </select>
                        </div>
                        <button type="button" id="submitsearch" class="btn btn-primary">Filter</button>
                    </form>
                    <div class="col-md-11" id="chart" style="height: 250px;"></div>
                </div>
                <div class="row">
                </div>
                <hr>
                <div style="margin-top:30px;" class="row">
                    <div class="col-lg-5">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Order Statistical
                                    <small></small>
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <div class="chart-container" style=" height:350px; width:350px">
                                    <canvas id="orderChart"></canvas>
                                </div>
                                <script>
                                    const ctx = document.getElementById('orderChart').getContext('2d');
                                    const myChart = new Chart(ctx, {
                                        type: 'doughnut',
                                        data: {
                                            labels: [
                                                'Waiting', 'Confirmed', 'Delivering', 'Received', 'Cancelled'
                                            ],
                                            datasets: [{
                                                label: 'My First Dataset',
                                                data: [{{ $order_waiting }}, {{ $order_confirmed }}, {{ $order_delivering }},
                                                    {{ $order_received }}, {{ $order_canceller }}
                                                ],
                                                backgroundColor: [
                                                    'rgb(255, 255, 51, 0.7)',
                                                    'rgb(121, 255, 77, 0.7)',
                                                    'rgb(77, 255, 255, 0.7)',
                                                    'rgb(255, 0, 255, 0.7)',
                                                    'rgb(255, 51, 51, 0.7)',
                                                ],
                                                hoverOffset: 4
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                y: {
                                                    beginAtZero: true
                                                }
                                            }
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Products Statistical
                                    <small></small>
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <div class="chart-container" style=" height:350px; width:700px">
                                    <canvas id="productChart"></canvas>
                                </div>
                                <script>
                                    const ctx2 = document.getElementById('productChart').getContext('2d');
                                    const productChart = new Chart(ctx2, {
                                        type: 'bar',
                                        data: {
                                            labels: [
                                                'Total',
                                                'Fruits',
                                                'Juice',
                                                'Vegetable',
                                                'Grain',
                                            ],
                                            datasets: [{
                                                label: 'Total Products',
                                                data: [{{ $total_product }}, {{ $pro_fruit }}, {{ $pro_juice }},
                                                    {{ $pro_vege }}, {{ $pro_grain }}
                                                ],
                                                backgroundColor: [
                                                    'rgba(255, 99, 132, 0.3)',
                                                    'rgba(255, 255, 0, 0.3)',
                                                    'rgba(102, 255, 102, 0.3)',
                                                    'rgba(75, 192, 192, 0.3)',
                                                    'rgba(75, 192, 192, 0.3)',
                                                ],
                                                borderColor: [
                                                    'rgb(255, 99, 132)',
                                                    'rgb(255, 255, 0)',
                                                    'rgb(102, 255, 102)',
                                                    'rgb(75, 192, 192)',
                                                    'rgba(75, 192, 192, 0.3)',
                                                ],
                                                borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                y: {
                                                    beginAtZero: true
                                                }
                                            }
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // dayOrder();
            var ppchart = new Morris.Area({
                element: 'chart',
                lineColors: ['#ff66d9', '#ffff1a'],
                pointFillColors: ['#ff3838'],
                pointStrokeColors: ['#ffffff'],
                // fillOpacity: 0.3,
                hideHover: 'auto',
                parseTime: false,
                xkey: ['created_at'],
                ykeys: ['total'],
                labels: ['Total'],
                // data: [{
                //         year: '2008',
                //         value: 20
                //     },
                //     {
                //         year: '2009',
                //         value: 10
                //     },
                // ],

            });
            $('#submitsearch').click(function() {
                var _token = $('input[name = "_token"]').val();
                var fromdate = $('#fromdate').val();
                var todate = $('#todate').val();
                $.ajax({
                    url: "{{ url('/filter-by-date') }}",
                    method: "POST",
                    data: {
                        fromdate: fromdate,
                        todate: todate,
                        _token: _token
                    },
                    dataType: "JSON",
                    success: function(data) {

                        ppchart.setData(data);
                    }
                });
            });

            $('#datefilter').change(function() {
                var dashboard_value = $(this).val();
                var _token = $('input[name = "_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ url('/dashboard-filter') }}",
                    dataType: "JSON",
                    data: {
                        dashboard_value: dashboard_value,
                        _token: _token
                    },
                    success: function(data) {
                        ppchart.setData(data);
                    }
                });
            });

        });
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    </script>
    {{-- <script>
        function dayOrder() {
            var _token = $('input[name = "_token"]').val();
            $.ajax({
                method: "POST"
                url: "{{ url('/day-order') }}",
                data: {
                    _token: _token
                },
                dataType: "JSON",
                success: function(data) {
                    chart.setData(data);
                }
            });
        }
    </script> --}}
@endsection
