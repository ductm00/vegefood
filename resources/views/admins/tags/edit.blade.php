@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('tag.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Tags List</a>
                <h6 class="m-0 font-weight-bold text-primary">Tag {{ $tag->name }} Edit</h6>
            </div>

            <form method="post" class="form-horizontal" action="{{ route('tag.update', [$tag->slug]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div style="text-align: center ; margin-top: 30px">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Name <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-7"><input type="text"
                                class="form-control name" name="name" onkeyup="ChangeToSlug(this)"
                                value="{{ old('name', $tag->name) }}"> <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('name')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Slug <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-7"><input type="text"
                                class="form-control convert_slug" name="slug" value="{{ old('slug', $tag->slug) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('slug')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div style="margin-left : 700px" class="col-lg-21">
                            <button class="btn btn-primary" type="submit">Update</button>
                            <a class="btn btn-info " href="{{ route('tag.index') }}">Back</a>
                        </div>
                    </div>
            </form>

        </div>
    </div>
    </div>
@endsection


