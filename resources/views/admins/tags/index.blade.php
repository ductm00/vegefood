@extends('layouts.admins.main')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                {{-- <h6 class="m-0 font-weight-bold text-primary">Home</h6> --}}
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('tag.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Tags List</a>
                {{-- <h6 style="display: flex" class="m-0 font-weight-bold text-primary">tags List</h6> --}}

            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                    action="{{ route('tag.index') }}" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" id="search" name="search" value="{{ Request::get('search') }}"
                            aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                            aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                        placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <div style="margin: 14px 0px 0px 0px" class="col-sm-2">
                        <div class="form-group">
                            <div style="display: flex;" class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-primary" href="" data-toggle="modal" data-target=".bd-example-modal-lg"><i
                                        class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </ul>



            </nav>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>{{ __('Slug')}}</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tags as $tag)
                                <tr>
                                    <td>{{ $tag->id }}</td>
                                    <td>{{ $tag->name }}</td>
                                    <td>{{ $tag->slug }}</td>
                                    <td>
                                        <div style="display: flex">
                                            <button style=" width: 35px; height: 35px;" type="button"
                                                class="btn btn-primary btn-sm"><a style="color: white ;"
                                                    href="{{ route('tag.edit', [$tag->slug]) }}"
                                                    class="ac-button">
                                                    <span class="far fa-edit"></span></a></button>

                                            <form action="{{ route('tag.destroy', [$tag->id]) }}"
                                                method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button style=" width: 35px; height: 35px; margin-left: 5px" type="submit"
                                                    class="btn btn-danger btn-sm"
                                                    onclick="return confirm('Are you sure you want to delete this tag ?')">
                                                    <span class="far fa-trash-alt"></span></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            {{ $tags->appends(request()->query())->links('vendor.pagination.custom') }}
        </div>
    </div>


    <!-- Extra large modal -->
    <div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create tags</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">



                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" id="form" action="{{ route('tag.store') }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div style="text-align: center">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Name <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control name" id="name" name="name" onkeyup="ChangeToSlug(this)"  value="{{ old('name') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('name')
                                    <span  class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="form-group"><label class="col-sm-2 control-label">{{ __('Slug')}} <i
                                            class="note"><span class="text-danger">*</span></i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control convert_slug" name="slug" value="{{ old('slug') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('slug')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror

                           
                            <div class="hr-line-dashed"></div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="submit" id="submit" name="submit">Create</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                            </div>
                        </form>
                        <div id="message"></div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
