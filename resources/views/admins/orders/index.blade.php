@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                {{-- <h6 class="m-0 font-weight-bold text-primary">Home</h6> --}}
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('order.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Order List</a>
                {{-- <h6 style="display: flex" class="m-0 font-weight-bold text-primary">Categories List</h6> --}}

            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                    action="{{ route('order.index') }}" method="get">

                    <div style="width:500px" class="input-group">

                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" id="search" name="search" value="{{ Request::get('search') }}"
                            aria-describedby="basic-addon2">
                        <div class="input-group-append col-lg-3">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>

                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                            aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                        placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <div style="margin: 14px 0px 0px 0px" class="col-sm-2">
                        <div class="form-group">
                            <div style="display: flex;" class="col-sm-4 col-sm-offset-2">
                                <a title="Export Data" style="margin-left: 5px;" class="btn btn-info" href="{{route('order.export')}}"><i
                                        class="fas fa-share-square"></i></a>
                            </div>
                        </div>
                    </div>
                </ul>



            </nav>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('User')}}</th>
                                <th>{{__('Total')}}</th>
                                <th>{{__('Payment')}} </th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($ordersxx)
                            <tr>
                                <td>{{ $ordersxx->id }}</td>
                                <td>{{ $ordersxx->user->name }}</td>
                                <td>{{ $ordersxx->total }}</td>
                                <td>{{ $ordersxx->payment_method }}</td>
                                <td>{{ $ordersxx->status }}</td>
                                <td>
                                    <div style="display: flex">
                                        <button style=" width: 35px; height: 35px;" type="button"
                                            class="btn btn-info btn-sm" data-toggle="modal"
                                            data-target="#detailModal{{ $ordersxx->id }}">
                                            <span class="far fa-edit"></span></button>

                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="detailModal{{ $ordersxx->id }}" tabindex="-1"
                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content">
                                        <form action="{{ route('order.update', [$ordersxx->id]) }}" method="post">
                                            @csrf
                                            @method('PUT')
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Order {{ $ordersxx->id }} </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                
                                                <select class="form-control " name="status" id="status">
                                                    @foreach ($status as $sta)
                                                        <option {{ $sta == $ordersxx->status ? 'selected' : '' }}>{{ $sta }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $order->user->name }}</td>
                                        <td>{{ $order->total }}</td>
                                        <td>{{ $order->payment_method }}</td>
                                        <td>{{ $order->status }}</td>
                                        <td>
                                            <div>
                                                <button style=" width: 35px; height: 35px;" type="button"
                                                    class="btn btn-info btn-sm" data-toggle="modal"
                                                    data-target="#detailModal{{ $order->id }}">
                                                    <span class="far fa-edit"></span></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="detailModal{{ $order->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-xl" role="document">
                                            <div class="modal-content">
                                                <form action="{{ route('order.update', [$order->id]) }}" method="post">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Order
                                                            {{ $order->id }} </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="tab-pane fade show active text-center" id="home"
                                                            role="tabpanel" aria-labelledby="home-tab">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label>Receiver City :</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p>{{ $order->city_name }}</p>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label>receiver District :</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p>{{ $order->district_name }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label>receiver Village :</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p>{{ $order->village_name }}</p>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label>receiver Address :</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p>{{ $order->receiver_address }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label>Total :</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p>{{ $order->total }}</p>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label>Payment :</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <p>{{ $order->payment_method }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <select class="form-control " name="status" id="status">
                                                            @foreach ($status as $sta)
                                                                <option {{ $sta == $order->status ? 'selected' : '' }}>{{ $sta }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $orders->appends(request()->get('search'))->links('vendor.pagination.custom') }}
        </div>
    </div>
@endsection

@section('script')
    @if (Request::get('order_id'))
        <script>
            $(document).ready(function() {
                let order_id = "{{ Request::get('order_id') }}";
                $('#detailModal' + order_id).modal('show');
            })

            $('#myModal').on('shown.bs.modal', function() {
                $('#myInput').trigger('focus')
            })

            $('#search').on('keyup', function() {
                $value = $(this).val();
                $.ajax({
                    type: 'get',
                    url: '{{ URL::to('order') }}',
                    data: {
                        'search': $value
                    },
                    success: function(data) {
                        $('tbody').html(data);
                    }
                });
            })
            $.ajaxSetup({
                headers: {
                    'csrftoken': '{{ csrf_token() }}'
                }
            });
        </script>
    @endif
@endsection
