@extends('layouts.admins.main')
@section('content')
<div class="card shadow mb-4">
    <div style="display: flex" class="card-header py-3">
        {{-- <h6 class="m-0 font-weight-bold text-primary">Home</h6> --}}
        <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
        <a href="{{ route('tag.index') }}" style="margin-left:5px; text-decoration: none"
            class="font-weight-bold text-primary">Profile</a>
        {{-- <h6 style="display: flex" class="m-0 font-weight-bold text-primary">tags List</h6> --}}

    </div>
    <div class="container emp-profile">
        
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        <img src="{{ $user->image }}" alt="" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h5>
                            {{ $user->name }}
                        </h5>
                        <br><br>
                
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="true">About</a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" id="editProfile" value="{{$user->id}}" class="profile-edit-btn" name="btnAddMore" data-toggle="modal"
                        data-target="#profileModal">Edit Profile</button>
                </div>
                <div class="modal fade bd-example-modal-lg" id="profileModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <form action="{{ route('admin.updateProfile',[$user->id])}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input type="hidden" id="id" name="id">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit profile</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div style="text-align: center; margin-top:30px">
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Name <i
                                                    class="note text-danger">*</i></label>
                                            <div style="display: inline-block" class="col-sm-9"><input type="text"
                                                    class="form-control" id="name" name="name" value="{{ old('name', $user->name) }}"> <span
                                                    class="help-block m-b-none"></span>
                                            </div>
                                        </div>
                                        @error('name')
                                            <span class="errors text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Email <i
                                                    class="note text-danger">*</i></label>
                                            <div style="display: inline-block" class="col-sm-9"><input type="text"
                                                    class="form-control" id="email" name="email"  disabled value="{{ old('email', $user->email) }}"> <span
                                                    class="help-block m-b-none"></span>
                                            </div>
                                        </div>
                                        @error('email')
                                            <span class="errors text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Phone <i
                                                    class="note text-danger">*</i></label>
                                            <div style="display: inline-block" class="col-sm-9"><input type="text"
                                                    class="form-control" id="phone" name="phone" value="{{ old('phone', $user->phone) }}"> <span
                                                    class="help-block m-b-none"></span>
                                            </div>
                                        </div>
                                        @error('phone')
                                            <span class="errors text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Password <i
                                                    class="note text-danger">*</i></label>
                                            <div style="display: inline-block" class="col-sm-9"><input type="password"
                                                    class="form-control" id="password" name="password"> <span
                                                    class="help-block m-b-none"></span>
                                            </div>
                                            <i style=" position: absolute; margin-top:15px" onclick="showPassword()" class="fa fa-eye"></i>
                                        </div>
                                        @error('password')
                                            <span class="errors text-danger">{{ $message }}</span>
                                        @enderror
                                        <div class="hr-line-dashed"></div>
                                        <div style="margin-left: -295px" class="form-group"><label
                                                class="col-sm-2 control-label">Image <i
                                                    class="note text-danger">*</i></label>
                                            <label style="margin-left: -5px;" class="image"><input type="file"
                                                    id="image" name="image" aria-label="File browser example">
                                                <span class="file-custom"></span>
                                            </label>
                                        </div>
                                        @error('image')
                                            <span class="errors text-danger">{{ $message }}</span>
                                        @enderror

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                 
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Role</label>
                                </div>
                                <div class="col-md-6">
                                    <p>Admin</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>User Id</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ $user->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ $user->name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ $user->email }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ $user->phone }}</p>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-md-6">
                                    <label>Profession</label>
                                </div>
                                <div class="col-md-6">
                                    <p>Web Developer and Designer</p>
                                </div>
                            </div> --}}
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Experience</label>
                                </div>
                                <div class="col-md-6">
                                    <p>Expert</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Hourly Rate</label>
                                </div>
                                <div class="col-md-6">
                                    <p>10$/hr</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Total Projects</label>
                                </div>
                                <div class="col-md-6">
                                    <p>230</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>English Level</label>
                                </div>
                                <div class="col-md-6">
                                    <p>Expert</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Availability</label>
                                </div>
                                <div class="col-md-6">
                                    <p>6 months</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Your Bio</label><br />
                                    <p>Your detail description</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <script>
        function showPassword() {
          var x = document.getElementById("password");
          if (x.type === "password") {
            x.type = "text";
          } else {
            x.type = "password";
          }
        }
        </script>
@endsection

