@section('content')
    @extends('layouts.admins.main')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Product {{ $product->name }} Edit</h6>
            </div>

            <form method="post" class="form-horizontal" id="form" action="{{ route('product.update', [$product->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div style="margin-top:30px">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Name <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="name" name="name" value="{{ old('name', $product->name) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('name')
                        <span class="errors text-danger text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Code <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="code" name="code" value="{{ old('code', $product->code) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('code')
                        <span class="errors text-danger text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Quantity Instock <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="quantity_instock" name="quantity_instock"
                                value="{{ old('quantity_instock', $product->quantity_instock) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('quantity_instock')
                        <span class="errors text-danger text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Price <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="price" name="price" value="{{ old('price', $product->price) }}">
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('price')
                        <span class="errors text-danger text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Promotion Price <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="promotion_price" name="promotion_price"
                                value="{{ old('promotion_price', $product->promotion_price) }}"> <span
                                class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('promotion_price')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Unit <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9"><input type="text" class="form-control"
                                id="unit" name="unit" value="{{ old('unit', $product->unit) }}">
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('unit')
                        <span class="errors text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Image <i
                                class="note text-danger">*</i></label>
                        <label style="margin-left: -5px;" class="image"><input type="file" id="image" name="image"
                                aria-label="File browser example">
                            <span class="file-custom"></span>
                        </label>
                    </div>
                    @error('image')
                        <span class="errors text-danger text-danger">{{ $message }}</span>
                    @enderror


                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label"> Description <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9">
                            <textarea class="form-control" id="description" name="description" rows="7"
                                cols="50">{{ old('description', $product->description) }}</textarea>
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                    @error('description')
                        <span class="errors text-danger text-danger">{{ $message }}</span>
                    @enderror

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label"> Category <i
                                class="note text-danger">*</i></label>
                        <div style="display: inline-block" class="col-sm-9">
                            <select class="form-control" name="category_id" id="category_id">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{$product->category_id == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group"><label class="col-sm-2 control-label"> {{ __('Tag') }} <i
                            class="note"></i></label>
                    <div style="display: inline-block" class="col-sm-9">
                        <select class="tag_id form-control" name="tag_id[]" multiple="multiple">
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->id }}" {{$product->tags->contains($tag->id) ? 'selected' : ''}}>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                        <span class="help-block m-b-none"></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div style="margin-left : 1000px" class="col-lg-11">
                        <button class="btn btn-primary" type="submit" name="update">Update</button>
                        <a class="btn btn-info " href="{{ route('product.index') }}">Back</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.tag_id').select2();
        });
    </script>
@endsection
