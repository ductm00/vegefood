@extends('layouts.admins.main')
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="card shadow mb-4">
            <div style="display: flex" class="card-header py-3">
                {{-- <h6 class="m-0 font-weight-bold text-primary">Home</h6> --}}
                <a style="text-decoration: none" href="{{ route('home.admin') }}" class="">Home /</a>
                <a href="{{ route('product.index') }}" style="margin-left:5px; text-decoration: none"
                    class="font-weight-bold text-primary">Products List</a>
                {{-- <h6 style="display: flex" class="m-0 font-weight-bold text-primary">Categories List</h6> --}}

            </div>

            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                    action="{{ route('product.index') }}" method="get">

                    <div style="width:900px" class="input-group">

                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                            aria-label="Search" id="search" name="search" value="{{ Request::get('search') }}"
                            aria-describedby="basic-addon2">
                        <div style="margin: 0px 0px 0px 10px" class="">
                            {{-- <label class="control-label" for="status">Status</label> --}}
                            <select name="category_id" id="category_id" class="form-control ">
                                <option selected disabled>Select Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {{ Request::get('category_id') == $category->id ? 'selected' : '' }}>
                                        {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div style="margin: 0px 0px 0px 10px" class="">
                            {{-- <label class="control-label" for="status">Status</label> --}}
                            <select name="status" id="status" class="form-control ">
                                <option selected disabled>Select Status</option>
                                <option id="instock" value="instock"
                                    {{ Request::get('status') == 'instock' ? 'selected' : '' }}>In Stock</option>
                                <option id='outstock' value="outstock"
                                    {{ Request::get('status') == 'outstock' ? 'selected' : '' }}> Out Stock</option>
                            </select>
                        </div>
                        <div class="input-group-append col-lg-3">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>

                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <div style="margin: 14px 0px 0px 0px" class="col-sm-2">
                        <div class="form-group">
                            <div style="display: flex;" class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-primary" href="" data-toggle="modal" data-target=".bd-example-modal-xl"><i
                                        class="fa fa-plus"></i></a>
                             
                            </div>
                        </div>
                    </div>
                </ul>



            </nav>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Tag</th>
                                <th>Image</th>
                                <th>Code</th>
                                <th>Quantity Instock</th>
                                <th>Unit</th>
                                <th>Price</th>
                                <th>Promotion Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if ($products)
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>
                                            @foreach ($product->tags as $tag)
                                                <span class="badge badge-secondary">{{ $tag->name }}</span>
                                            @endforeach
                                        </td>
                                        <td><img width="80px" height="50px" src="{{ $product->image }}" alt=""></td>
                                        <td>{{ $product->code }}</td>
                                        <td>{{ $product->quantity_instock }}</td>
                                        <td>{{ $product->unit }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->promotion_price }}</td>
                                        <td>
                                            <div style="display: flex">
                                                <button style=" width: 35px; height: 35px;" type="button"
                                                    class="btn btn-primary btn-sm"><a style="color: white ;"
                                                        href="{{ route('product.edit', [$product->id]) }}"
                                                        class="ac-button">
                                                        <span class="far fa-edit"></span></a></button>

                                                <form action="{{ route('product.destroy', [$product->id]) }}"
                                                    method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button style=" width: 35px; height: 35px; margin-left: 5px"
                                                        type="submit" class="btn btn-danger btn-sm"
                                                        onclick="return confirm('Are you sure you want to delete this product ?')">
                                                        <span class="far fa-trash-alt"></span></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif


                        </tbody>
                    </table>
                </div>
            </div>
            <div>
                <div>
                    {{ $products->appends(request()->query())->links('vendor.pagination.custom') }}
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Products</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" id="form" action="{{ route('product.store') }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Name <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="name" name="name" value="{{ old('name') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('name')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Code <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="code" name="code" value="{{ old('code') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('code')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Quantity Instock <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="quantity_instock" name="quantity_instock"
                                            value="{{ old('quantity_instock') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('quantity_instock')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Price <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="price" name="price" value="{{ old('price') }}">
                                        <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('price')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Promotion Price <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="promotion_price" name="promotion_price"
                                            value="{{ old('promotion_price') }}"> <span
                                            class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('promotion_price')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Unit <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9"><input type="text"
                                            class="form-control" id="unit" name="unit" value="{{ old('unit') }}">
                                        <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('unit')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror

                                <div class="hr-line-dashed"></div>
                                <div style="margin-left: 20px" class="form-group"><label
                                        class="col-sm-2 control-label">Image <i class="note text-danger">*</i></label>
                                    <label style="margin-left: -5px;" class="image"><input type="file" id="image"
                                            name="image" aria-label="File browser example">
                                        <span class="file-custom"></span>
                                    </label>
                                </div>
                                @error('image')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror


                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label"> Description <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9">
                                        <textarea class="form-control" id="description" name="description" rows="7" cols="50"></textarea>
                                        <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('description')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror

                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label"> Category <i
                                            class="note text-danger">*</i></label>
                                    <div style="display: inline-block" class="col-sm-9">
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                                @error('promotion_price')
                                    <span class="errors text-danger">{{ $message }}</span>
                                @enderror
                                <div class="form-group"><label class="col-sm-2 control-label"> {{ __('Tag') }} <i
                                            class="note"></i></label>
                                    <div style="display: inline-block" class="col-sm-9">
                                        <select class="tag_id form-control" name="tag_id[]" multiple="multiple">
                                            @foreach ($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block m-b-none"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="submit" id="submit" name="submit">Create</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.tag_id').select2();
        });
        $('#myModal').on('shown.bs.modal', function() {
            $('#myInput').trigger('focus')
        })

        $('#search').on('keyup', function() {
            var category_id = $('#category_id').val();
            var status = $('#status').val();
            $value = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ URL::to('product') }}',
                data: {
                    'search': $value,
                    category_id: category_id,
                    status: status,
                },
                success: function(data) {
                    $('tbody').html(data);
                }
            });
        })
        $.ajaxSetup({
            headers: {
                'csrftoken': '{{ csrf_token() }}'
            }
        });
    </script>
@endsection
