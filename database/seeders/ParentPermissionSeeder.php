<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParentPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'Login admin',
                'parent_id' => '0',
                'key_code' => 'admin',
            ],
            [
                'name' => 'Super Admin',
                'parent_id' => '0',
                'key_code' => 'super-admin',
            ],
            [
                'name' => 'Category',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'Tag',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'Product',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'Permission',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'Coupon',
                'parent_id' => '1',
                'key_code' => '',

            ],
            [
                'name' => 'Order',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'Permisson',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'Role',
                'parent_id' => '1',
                'key_code' => '',
            ],
            [
                'name' => 'User',
                'parent_id' => '1',
                'key_code' => '',
            ],
        ]
        );
    }
}
