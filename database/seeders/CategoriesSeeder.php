<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Trái cây',
            'slug' => 'trai-cay',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649818233/dlguzh4uxczgojp61zbz.jpg',
            'public_id' => 'dlguzh4uxczgojp61zbz',
        ]);
        DB::table('categories')->insert([
            'name' => 'Nước ép',
            'slug' => 'nuoc-ep',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649818292/gsqjq6klqok29rzrs030.jpg',
            'public_id' => 'gsqjq6klqok29rzrs030',
        ]);
        DB::table('categories')->insert([
            'name' => 'Rau củ',
            'slug' => 'rau-cu',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649818270/eifirm15jeqyh0s6k6tg.jpg',
            'public_id' => 'eifirm15jeqyh0s6k6tg',
        ]);
        DB::table('categories')->insert([
            'name' => 'Hạt',
            'slug' => 'hat',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649818310/zkmvax9y9qyybt2zuvwv.jpg',
            'public_id' => 'zkmvax9y9qyybt2zuvwv',
        ]);
    }
}
