<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'image'=> 'https://res.cloudinary.com/dlsnhyxji/image/upload/v1653725415/avatar-default_yvxhpv.jpg',
            'password' => Hash::make('12345678'),
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1653662523/vzwjrggqqodn5gqacm1q.png',
            'public_id' => 'vzwjrggqqodn5gqacm1q',
        ]);

        // DB::table('users')->insert([
        //     'name' => 'Tran Manh Khiem',
        //     'email' => 'khiemtran@gmail.com',
        //     'password' => Hash::make('12345678'),
        // ]);
    }
}
