<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'code' => 'PTT01',
            'name' => 'Cà chua',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '25000',
            'promotion_price' => '20000',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649745459/rzf4dh20lg0l7lzxjehg.jpg',
            'public_id' => 'p6vtzewllgcwfz2ifnyp',
            'description' => 'Cà chua sạch từ những nông trại chuẩn Vietgab',
            'category_id' => '3',
        ]);
        DB::table('products')->insert([
            'code' => 'GP01',
            'name' => 'Ớt chuông',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '66000',
            'promotion_price' => '60000',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649820475/rp2zh0rhg2fqvjhybp4k.jpg',
            'public_id' => 'rp2zh0rhg2fqvjhybp4k',
            'description' => 'Ớt chuông sạch từ những nông trại chuẩn Vietgab',
            'category_id' => '3',
        ]);
        DB::table('products')->insert([
            'code' => 'DT01',
            'name' => 'Dâu tây',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '7000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649820506/mhxmvshzrhmld6d2utf0.jpg',
            'public_id' => 'mhxmvshzrhmld6d2utf0',
            'description' => 'Dâu tây sạch từ những nông trại chuẩn Vietgab',
            'category_id' => '1', 
        ]);
        DB::table('products')->insert([
            'code' => 'DHL01',
            'name' => 'Đậu hà lan',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '20000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821427/zxqbm6udfchwwql6xtbi.jpg',
            'public_id' => 'zxqbm6udfchwwql6xtbi',
            'description' => 'Đậu hà lan sạch từ những nông trại chuẩn Vietgab',
            'category_id' => '3', 
        ]);
        DB::table('products')->insert([
            'code' => 'BCT01',
            'name' => 'Bắp cải tím',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '40000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821446/vdjmkf0pxgzz1oc9ws69.jpg',
            'public_id' => 'vdjmkf0pxgzz1oc9ws69',
            'description' => 'Bắp cải tím sạch từ những nông trại chuẩn Vietgab',
            'category_id' => '3',
        ]);
        DB::table('products')->insert([
            'code' => 'SL01',
            'name' => 'Súp lơ',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '60000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821470/s62ikqftirhw2ny4frjk.jpg',
            'public_id' => 's62ikqftirhw2ny4frjk',
            'description' => 'Súp lơ sạch từ những nông trại chuẩn Vietgab',
            'category_id' => '3',
        ]);
        DB::table('products')->insert([
            'code' => 'NCR01',
            'name' => 'Nước ép cà rốt',
            'quantity_instock' => '10',
            'unit' => 'Chai',
            'price' => '30000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821536/qe9figk2lpanxok2insc.jpg',
            'public_id' => 'qe9figk2lpanxok2insc',
            'description' => 'Nước ép từ những củ cà rốt tươi ngon',
            'category_id' => '2', 
        ]);
        DB::table('products')->insert([
            'code' => 'HT01',
            'name' => 'Hành tím',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '30000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821556/ygo4ghdcik2m9chnbapn.jpg',
            'public_id' => 'ygo4ghdcik2m9chnbapn',
            'description' => 'Hành tím tươi ngon từ nông trại sạch',
            'category_id' => '3',
        ]);
        DB::table('products')->insert([
            'code' => 'TM01',
            'name' => 'Táo Mỹ',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '80000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821571/jqt2yl4ksb4smvqo5wro.jpg',
            'public_id' => 'jqt2yl4ksb4smvqo5wro',
            'description' => 'Táo nhập khẩu tươi ngon',
            'category_id' => '1',
        ]);
        DB::table('products')->insert([
            'code' => 'T01',
            'name' => 'Tỏi Lý Sơn',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '60000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821592/yahc5b0iihpnwl6ehi6a.jpg',
            'public_id' => 'yahc5b0iihpnwl6ehi6a',
            'description' => 'Tỏi Lý Sơn ngon',
            'category_id' => '4',
        ]);
        DB::table('products')->insert([
            'code' => 'OCT01',
            'name' => 'Ớt chỉ thiên',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '50000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821617/zyquprj8pmebzkngnqtn.jpg',
            'public_id' => 'zyquprj8pmebzkngnqtn',
            'description' => 'Ớt siêu cay khổng lồ',
            'category_id' => '4',
        ]);
        DB::table('products')->insert([
            'code' => 'CR01',
            'name' => 'Cà rốt',
            'quantity_instock' => '10',
            'unit' => 'kg',
            'price' => '39000',
            'promotion_price' => '',
            'image' => 'https://res.cloudinary.com/suppermarket/image/upload/v1649821511/afyehjs1dkjbmww85ky8.jpg',
            'public_id' => 'afyehjs1dkjbmww85ky8',
            'description' => 'Cà rốt siêu ngon khổng lồ',
            'category_id' => '3',
        ]);
    }
}
