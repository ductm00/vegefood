<?php
use App\Models\Notify;
use Carbon\Carbon;

if (!function_exists('countNotifications')) {
    function countNotifications()
    {
        return Notify::where('status','0')->count();
    }
}

if (!function_exists('getNotifications')) {
    function getNotifications()
    {
        return Notify::where('status','0')->orderBy('id','DESC')->get();
    }
}

