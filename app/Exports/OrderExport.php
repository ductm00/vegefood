<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrderExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Order::all();
    }

    public function headings(): array
    {
        return ['ID', 'Receiver phone','Receiver name','Receiver email','Receiver city','Receiver district','Receiver village','Receiver address','Total', 'Status', 'Payment method', 'User id', 'Coupon code', 'Created at', 'Update at'];
    }
}
