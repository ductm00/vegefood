<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;
use Nette\Utils\Random;
use Illuminate\Support\Str;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $data = $request->all();
            $token = strtoupper(Str::random(10));
            $data['token'] = $token;
            $user = User::where('email',$request->email)->first();
            $user->fill($data)->save();
            // dd(Crypt::decrypt($user->password));
            return $this->from('vegefoodmanager@gmail.com')
                ->view('mails.mail-notify',compact('user','token'))
                ->subject('Notification email');
    }
}