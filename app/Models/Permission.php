<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'parent_id', 'key_code'];

    public function roles()
    {

        return $this->belongsToMany(Role::class);
    }

    public function childrentPermissions()
    {

        return $this->hasMany(Permission::class, 'parent_id', 'id');
    }
}