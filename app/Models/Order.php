<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Order_Product;
use App\Models\User;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'receiver_name',
        'receiver_phone',
        'receiver_email',
        'receiver_city',
        'receiver_district',
        'receiver_village',
        'receiver_address',
        'status',
        'total',
        'payment_method',
        'user_id',
        'coupon_code',
        'city_name',
        'district_name',
        'village_name',
    ];

    public function products() {
        return $this->belongsToMany(Product::class)->withPivot('quantity', 'price');
    }

    public function order_product() {
        return $this->hasMany(Order_Product::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public static function status(){
        return ['Waiting','Confirmed','Delivering','Received','Cancelled'];
    }

    public function payment_method(){
        return ['COD','Vnpay'];
    }
}
