<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Coupon extends Model
{
    use HasFactory;

    protected $primaryKey = 'code';
    public $incrementing = false;
    
    protected $keyType = 'string';

    protected $fillable = [
        'code',
        'value',
        'start_date',
        'end_date'
    ];

    const PAGINATE = '10';

    public function users()
    {
        return $this->belongsToMany(User::class, 'coupon_user', 'user_id', 'coupon_code');
    }
}
