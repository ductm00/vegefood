<?php

namespace App\Models;

// use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Product;
use App\Models\Coupon;
use App\Models\Order;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'city',
        'district',
        'village',
        'token',
        'address',
        'image',
        'punlic_id',
        'google_id',
        'facebook_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   
    public function products() {
        return $this->belongsToMany(Product::class,'carts','user_id','product_id')->withPivot('id','quantity', 'price');
    }

    public function likes() {
        return $this->belongsToMany(Product::class,'likes','user_id','product_id');
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class,'coupon_user','user_id','coupon_code');
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function checkPermissionAccess($permissionCheck) 
    {
        $roles = auth()->user()->roles;
        foreach($roles as $role) {
            $permissions = $role->permissions;
            if($permissions->contains('key_code',$permissionCheck) or $permissions->contains('key_code','super-admin')) {
                return true;
            }
        }
        return false;
    }
}
