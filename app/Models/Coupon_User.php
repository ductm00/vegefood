<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon_User extends Model
{
    use HasFactory;
    
    protected $table = 'coupon_user';

    protected $fillable = ['coupon_code','user_id'];
}
