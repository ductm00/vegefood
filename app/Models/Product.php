<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\User;
use App\Models\Comment;

class Product extends Model
{
    use HasFactory;
    
    protected $fillable = ['code','name','quantity_instock', 'unit', 'price', 'promotion_price', 'image', 'public_id', 'description', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
   
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function products() {
        return $this->belongsToMany(User::class,'carts','user_id','product_id')->withPivot('id','quantity', 'price');
    }

    public function orders() {
        return $this->belongsToMany(Order::class)->withPivot('quantity');
    }

    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

    public function likes() {
        return $this->belongsToMany(User::class,'likes','product_id','user_id');
    }

}

