<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Cart;
use App\Models\Like;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['users.*'], function ($view) {
            if(auth()->check()) {
                $quantity_in_cart = Cart::where('user_id', auth()->user()->id)->sum('quantity');
            }
            else {
                $quantity_in_cart = 0;
            }

            $view->with(['quantity_in_cart' => $quantity_in_cart]);
        });

        View::composer(['users.*'], function ($view) {
            if(auth()->check()) {
                $quantity_in_wishlist = Like::where('user_id', auth()->user()->id)->count();
            }
            else {
                $quantity_in_wishlist = 0;
            }

            $view->with(['quantity_in_wishlist' => $quantity_in_wishlist]);
        });
    }
}
