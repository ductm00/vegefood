<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // VerifyEmail::toMailUsing(function ($notifiable, $url) {
        //     return (new MailMessage)
        //         ->subject('Verify Email Address')
        //         ->line('Click the button below to verify your email address.')
        //         ->action('Verify Email Address', $url);
        // });

        Gate::define('category-list', function ($user) {

            return $user->checkPermissionAccess(config('permissions')['access']['list_Category']);
        });
        Gate::define('category-add', function ($user) {

            return $user->checkPermissionAccess(config('permissions')['access']['add_Category']);
        });
        Gate::define('category-edit', function ($user) {

            return $user->checkPermissionAccess(config('permissions')['access']['edit_Category']);
        });
        Gate::define('category-delete', function ($user) {

            return $user->checkPermissionAccess(config('permissions')['access']['delete_Category']);
        });

        //Product

        Gate::define('product-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['list_Product']);
        });
        Gate::define('product-add', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['add_Product']);
        });
        Gate::define('product-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['edit_Product']);
        });
        Gate::define('product-delete', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['delete_Product']);
        });

        //Tag

        Gate::define('tag-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['list_Tag']);
        });
        Gate::define('tag-add', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['add_Tag']);
        });
        Gate::define('tag-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['edit_Tag']);
        });
        Gate::define('tag-delete', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['delete_Tag']);
        });

        //Permission
        Gate::define('permission-add', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['add_Permission']);
        });

        //Coupon
        Gate::define('coupon-add', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['add_Coupon']);
        });
        Gate::define('coupon-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['edit_Coupon']);
        });
        Gate::define('coupon-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['list_Coupon']);
        });
        Gate::define('coupon-delete', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['delete_Coupon']);
        });

        //Order

        Gate::define('order-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['list_Order']);
        });

        Gate::define('order-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['edit_Order']);
        });

        //Role
        Gate::define('role-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['list_Role']);
        });

        Gate::define('role-add', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['add_Role']);
        });
        Gate::define('role-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['edit_Role']);
        });
        Gate::define('role-delete', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['delete_Role']);
        });
        //User

        Gate::define('user-list', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['list_User']);
        });

        Gate::define('user-edit', function ($user) {
            return $user->checkPermissionAccess(config('permissions')['access']['edit_User']);
        });


    }
}
