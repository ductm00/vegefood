<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class SubscriberController extends Controller
{
    public function sendMail(Request $request)
    {

        $isExist = User::select("*")
            ->where("email", $request->email)
            ->exists();

        if ($isExist) {
            $user = User::where('email', $request->email)->first();
            // ->update(['password' => Crypt::encrypt('1234567')]);
            $to_email = User::select('email')->where('email', $request->email)->first();
            Mail::to($to_email)->send(new SendMail);
        } else {
            return back()->with('error', 'User email not found, please re-enter emai ! ');
        }
        return back()->with('success', 'Email sent successfully, please check your email.');



        // $to_email = "nbhung278@gmail.com";

        // Mail::to($to_email)->send(new SendMail);

        // return "<p> Thành công! Email của bạn đã được gửi</p>";

    }
}
