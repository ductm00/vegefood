<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCheckoutRequest;
use App\Models\Coupon_User;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Models\Order_Product;
use App\Models\Cart;
use Pusher\Pusher;
use App\Models\Notify;
use Carbon\Carbon;
use Exception;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();
        $list_products = $data['list'];
        foreach ($list_products as $product) {
            if (isset($product['id'])) {
                $quantity_instock = Product::find($product['id'])->quantity_instock;
                if ($product['quantity'] > $quantity_instock) {
                    return redirect()->back()->with('error', 'Số lượng sản phẩm trong kho không đủ');
                }
            }
        }
        $coupon_value = $data['coupon_value'];
        $coupon_code = $data['coupon_code'];
        $respone =  Http::withHeaders(['token' => '82ee90d5-be69-11ec-8bf9-6e703843c1f8'])->get('https://online-gateway.ghn.vn/shiip/public-api/master-data/province')->json();
        $cities = $respone['data'];

        return view('users.checkout', compact('cities', 'list_products', 'coupon_value', 'coupon_code'));
    }

    public function feeShip(Request $request)
    {

        $data = $request->all();
        $respone = Http::withHeaders(['token' => '82ee90d5-be69-11ec-8bf9-6e703843c1f8', 'shop_id' => '2661244'])->get('https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/fee', ['insurance_value' => $data['insurance_value'], 'service_type_id' => $data['service_type_id'], 'coupon' => '', 'to_ward_code' => $data['to_ward_code'], 'to_district_id' => $data['to_district_id'], 'from_district_id' => $data['from_district_id'], 'weight' => $data['weight'], 'length' => $data['length'], 'width' => $data['width'], 'height' => $data['height']])->json();

        return response()->json([
            'feeship' => $respone['data']
        ]);
    }

    public function order(UserCheckoutRequest $request)
    {

        $data = $request->all();

        $data['status'] = 'Waiting';
        $data['user_id'] = Auth::user()->id;
        $data['coupon_code'] = strtoupper($data['coupon_code']);

        $order = Order::create($data);
        $order_id = $order->id;
        if ($data['coupon_code']) {
            Coupon_User::create($data);
        }

        foreach ($data['list'] as $product) {
            Product::find($product['id'])->decrement('quantity_instock', number_format($product['quantity']));
            Cart::where('user_id', Auth::user()->id)->where('product_id', $product['id'])->delete();
            $order_id = Order::orderBy('id', 'DESC')->first()->id;
            $order_product = new Order_Product;
            $order_product->order_id = $order_id;
            $order_product->product_id = $product['id'];
            $order_product->quantity = $product['quantity'];
            $order_product->price = $product['price'];
            $order_product->save();
        }
        $notify = [
            'user_id' => Auth::user()->id,
            'order_id' => $order_id,
            'title' => 'Đơn hàng vừa được tạo',
            'status' => '0',
            'content' => 'Đơn hàng ' . $order_id . ' vừa được tạo bởi người dùng ' . Auth::user()->name . ' đang chờ xử lý',
        ];

        try {
            Notify::create($notify);
        } catch (Exception $e) {
            dd($e->getMessage());
        }


        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $pusher->trigger('development', 'server.created', [
            'title' => 'Đơn hàng vừa được tạo',
            'created_at' => Carbon::now()->toDateString(),
            'message' => 'Đơn hàng ' . $order_id . ' vừa được tạo bởi người dùng ' . Auth::user()->name . ' đang chờ xử lý',
        ]);


        if ($data['payment_method'] == 'vnpay') {
            $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
            $vnp_Returnurl = "http://prod.vegefoods.tk";
            $vnp_TmnCode = "R3HM05CK"; //Mã website tại VNPAY 
            $vnp_HashSecret = "QMGZGRPEJOPMSMTCRDBIYCVSLIRXVPRE"; //Chuỗi bí mật

            $vnp_TxnRef = $order_id; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
            $vnp_OrderInfo = 'Thanh toán Vnpay demo';
            $vnp_OrderType = 'billpayment';
            $vnp_Amount = $data['total'] * 100;
            $vnp_Locale = 'vn';
            $vnp_BankCode = 'NCB';
            $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

            $inputData = array(
                "vnp_Version" => "2.1.0",
                "vnp_TmnCode" => $vnp_TmnCode,
                "vnp_Amount" => $vnp_Amount,
                "vnp_Command" => "pay",
                "vnp_CreateDate" => date('YmdHis'),
                "vnp_CurrCode" => "VND",
                "vnp_IpAddr" => $vnp_IpAddr,
                "vnp_Locale" => $vnp_Locale,
                "vnp_OrderInfo" => $vnp_OrderInfo,
                "vnp_OrderType" => $vnp_OrderType,
                "vnp_ReturnUrl" => $vnp_Returnurl,
                "vnp_TxnRef" => $vnp_TxnRef
            );

            if (isset($vnp_BankCode) && $vnp_BankCode != "") {
                $inputData['vnp_BankCode'] = $vnp_BankCode;
            }
            ksort($inputData);
            $query = "";
            $i = 0;
            $hashdata = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
                } else {
                    $hashdata .= urlencode($key) . "=" . urlencode($value);
                    $i = 1;
                }
                $query .= urlencode($key) . "=" . urlencode($value) . '&';
            }

            $vnp_Url = $vnp_Url . "?" . $query;
            if (isset($vnp_HashSecret)) {
                $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret); //  
                $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
            }
            $returnData = array(
                'code' => '00', 'message' => 'success', 'data' => $vnp_Url
            );
            header('Location: ' . $vnp_Url);
            die();
        }

        return redirect()->route('homepage')->with('success', 'Bạn đã đặt hàng thành công');
    }
}
