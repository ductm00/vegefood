<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;
use App\Models\Product;
use Exception;
use App\Models\Coupon;
use App\Models\Coupon_User;
use Carbon\Carbon;

class CartController extends Controller
{
    public function show()
    {
        $carts = User::where('id', Auth::user()->id)->first();

        return view('users.cart', compact('carts'));
    }

    public function addToCart(Request $request)
    {
        $data = $request->all();
        if (isset($data['user_id'])) {
            $data['quantity'] = 1;
        }
        $product_in_cart = Cart::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();
        $quantity = Cart::where('user_id', Auth::user()->id)
            ->where('product_id', $data['product_id'])->first();
        $new_quantity = $data['quantity'];
        if ($quantity != null) {
            $new_quantity =  number_format($quantity->quantity) + $new_quantity;
        }

        $cart = User::where('id', Auth::user()->id)->first();

        $product = Product::find($data['product_id']);
        $product_in_cartt = Cart::where('user_id', Auth::user()->id)->where('product_id', $data['product_id'])->first();
        if ($product_in_cartt) {
            $this_quantity_in_cart = $product_in_cartt->quantity;
        }

        if ($new_quantity > $product->quantity_instock) {
            if($product->quantity_instock != 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'Giỏ hàng của bạn đã có ' . $this_quantity_in_cart . ' sản phẩm.',
                    'this_quantity_in_cart' => $this_quantity_in_cart
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Sản phẩm hiện tại trong kho đã hết',
                ]);
            }
           
        } else {
            if (in_array($data['product_id'], $product_in_cart)) {
                Cart::where('user_id', Auth::user()->id)->where('product_id', $data['product_id'])->update(['quantity' => $new_quantity]);
            } else {

                $cart->products()->attach($product->id, ['quantity' => $new_quantity, 'price' => $product->price]);
            }


            $quantity_in_cart = Cart::where('user_id', Auth::user()->id)->sum('quantity');


            return response()->json([
                'status' => true,
                'quantity_in_cart' => $quantity_in_cart,
            ]);
        }
    }


    public function update(Request $request)
    {

        $data = $request->all();
        Cart::find($data['id'])->update(['quantity' => $data['quantity_update']]);
        $quantity_in_cart = Cart::where('user_id', Auth::user()->id)->sum('quantity');

        return response()->json([
            'success' => 'Ajax request submitted successfully',
            'quantity_in_cart' => $quantity_in_cart
        ]);
    }

    public function deleteProduct(Request $request)
    {
        $data = $request->all();

        Cart::find($data['id'])->delete();
        $quantity_in_cart = Cart::where('user_id', Auth::user()->id)->sum('quantity');


        return response()->json([
            'success' => 'Ajax request submitted successfully',
            'quantity_in_cart' => $quantity_in_cart
        ]);
    }

    public function checkCoupon(Request $request)
    {

        $data = $request->all();
        $now_date = Carbon::now()->toDateString();
        $coupons_code = Coupon::find($data['coupon_code']);
        $coupons_used = Coupon_User::where('user_id', Auth::user()->id)->pluck('coupon_code')->toArray();

        if ($coupons_code) {
            if (in_array(strtoupper($data['coupon_code']), $coupons_used)) {

                return response()->json(['message' => 'Bạn đã sử dụng mã giảm giá này', 'coupon_value' => '0']);
            } elseif ($now_date > $coupons_code->end_date) {

                return response()->json(['message' => 'Mã giảm giá đã hết hạn sử dụng', 'coupon_value' => '0']);
            } elseif ($now_date < $coupons_code->start_date) {

                return response()->json(['message' => 'Mã giảm giá có hiệu lực từ ngày ' . ($coupons_code->start_date), 'coupon_value' => '0']);
            } else {

                return response()->json(['message' => 'Bạn đã áp dụng mã giảm giá thành công', 'coupon_value' => $coupons_code->value]);
            }
        } else {

            return response()->json(['message' => 'Mã giảm giá không tồn tại', 'coupon_value' => '0']);
        }
    }
}
