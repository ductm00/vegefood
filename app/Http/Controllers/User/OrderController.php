<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index() {
        
        $orders = Order::where('user_id',Auth::user()->id)->get();

        return view('users.order',compact('orders'));
    }

    public function show(Request $request) {
        $order = Order::find($request->order_id);

        if(isset($request->status)) {
            $order->update(['status' => $request->status]);
            
            return response()->json(['success'=> true]);
        }
    }
}
