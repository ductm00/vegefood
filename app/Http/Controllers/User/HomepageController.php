<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Comment;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Services\ProductService;
use App\Services\TagService;
use App\Services\CategoryService;
use App\Models\Like;

class HomepageController extends Controller
{
    protected $productService;
    protected $tagService;
    protected $categoryService;

    public function __construct(ProductService $productService, TagService $tagService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->tagService = $tagService;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $products = Product::orderby('id', 'DESC');
        
        if (isset($data['tag'])) {
            $tag_id = $this->tagService->findTagBySlug($data['tag'])->id;
            $products_id = $this->tagService->getProductsbyTag($tag_id)->pluck('id')->toArray();
            $products = $products->whereIn('id', $products_id);
        }

        if (isset($data['category'])) {
            $category_id = $this->categoryService->getIdBySlug($data['category']);
            $products = $products->where('category_id', $category_id);
        }

        if(isset($data['search'])) {
            $products = $products->where(function($query) use($data){
                $query->where('name','like','%'.$data['search'].'%')
                ->orWhere('code', $data['search'])
                ->orWhere('description', 'like', '%'. $data['search'] . '%');
            });
        }   
        
          $products = $products->paginate(config('paginate.paginate'));
        
        $categories = Category::orderBy('id', 'DESC')->get();

        return view('users.homepage', compact('products', 'categories'));
    }

    public function showProduct($id)
    {

        $product = Product::find($id);
        $relate_products = $product->category->products->slice(0, 4);
        $comments = Comment::where('product_id', $id)->orderBy('id', 'DESC')->get();
        $avg_star = round(Comment::where('product_id', $id)->avg('rating'), 1);
        $star_sum = Comment::where('product_id', $id)->count('rating');

        $quantity_instock = Product::find($id)->quantity_instock;

        $tags = $this->productService->getTagsByProductId($id);

        return view('users.showproduct', compact('product', 'comments', 'avg_star', 'star_sum', 'quantity_instock', 'tags', 'relate_products'));
    }

    public function storeComment(Request $request, $id)
    {
        try {
            $data = $request->all();

            $data['product_id'] = $id;
            $data['user_id'] = Auth::user()->id;

            Comment::create($data);

            return redirect()->back();
        } catch (Exception $e) {

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function showWishlist()
    {
        return view('users.wishlist');
    }

    public function addToWishlist(Request $request)
    {
        $data = $request->all();
        if(Auth::user()->likes->contains($data['product_id'])) {
            return response()->json([
                'status' => false,
                'message' => 'Sản phẩm đã có trong danh sách yêu thích'
            ]);
        }
        else {
        Product::find($data['product_id'])->likes()->attach($data['user_id']);
        $quantity_in_wishlist = Like::where('user_id', auth()->user()->id)->count();

        return response()->json([
            'status' => true,
            'message' => 'Sản phẩm đã thêm vào danh sách yêu thích',
            'quantity_in_wishlist' => $quantity_in_wishlist,
        ]);
    }   
    }
    public function deleteProduct(Request $request, $id)
    {
        $data = $request->all();
        // Like::find($data['id'])->delete();
        Auth::user()->likes()->detach($data['id']);
        $quantity_in_wishlist = Like::where('user_id', auth()->user()->id)->count();

        return response()->json([
            'success' => 'Ajax request submitted successfully',
            'quantity_in_wishlist' => $quantity_in_wishlist
        ]);
    }

    public function about()
    {
        return view('users.about');
    }

    public function contact()
    {
        return view('users.contact');
    }

    public function blog()
    {
        return view('users.blog');
    }

    public function showProfile($id)
    {
        $user = auth()->user();
        return view('users.profile', compact('user'));
    }
}
