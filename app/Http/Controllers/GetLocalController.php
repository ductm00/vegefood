<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class GetLocalController extends Controller
{
    
    public function district(Request $request)
    {
         
        $ProvinceID = $request->ProvinceID;

        $respone = Http::withHeaders(['token' => '82ee90d5-be69-11ec-8bf9-6e703843c1f8', 'contentType' => 'application/json; charset=utf-8',])->get('https://online-gateway.ghn.vn/shiip/public-api/master-data/district', ['province_id' => $ProvinceID])->json();

        $districts = $respone['data'];
        // $districts = DB::table('districts')->where('matp',$city_id)
        //                       ->get();

        return response()->json([
            'districts' => $districts
        ]);
    }

    public function village(Request $request)
    {
         
        $district_id = $request->district_id;

        $respone = Http::withHeaders(['token' => '82ee90d5-be69-11ec-8bf9-6e703843c1f8', 'contentType' => 'application/json; charset=utf-8',])->get('https://online-gateway.ghn.vn/shiip/public-api/master-data/ward', ['district_id' => $district_id])->json();

        // $villages = DB::table('villages')->where('maqh',$district_id)
        //                       ->get();

        $villages = $respone['data'];
        return response()->json([
            'villages' => $villages
        ]);
    }
}
