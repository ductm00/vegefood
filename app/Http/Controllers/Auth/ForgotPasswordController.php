<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function getPassword($id , $token)
    {
        $user = User::find($id);
        if ($user->token == $token) {
        return view('auth.passwords.confirm', compact('user'));
        }
        return abort(404);
    }

    public function setPassword(Request $request)
    {
        $data = $request->all();
        $user = User::find($data['id']);
        $user->update(['password' => Hash::make($data['password'])]);
        return redirect()->route('login')->with('success', 'Bạn đã đổi mật khẩu thành công');
    }
}
