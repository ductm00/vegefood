<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\RoleService;
use Exception;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;

        $this->middleware('can:user-list')->only('index');
        $this->middleware('can:user-edit')->only('update');
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $users = $this->userService->showUser($data);

        $roles = $this->roleService->listRoles();
        
        return view('admins.users.index', compact('users', 'roles'));
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            
            $user = $this->userService->getUserById($id);
            DB::beginTransaction();

            $user->roles()->detach();

            $user->roles()->attach($data['role_id']);

            DB::commit();

            return redirect()->route('user.index')->with('success', 'Cấp quyền cho user thành công');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('user.index')->with('error', $e->getMessage());
        }
    }
}
