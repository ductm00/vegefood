<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PermissionService;
use Exception;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->middleware('can:permission-add')->only('create', 'store');
    }

    public function create()
    {
        $parentPermission = $this->permissionService->getParentPermissions();

        return view('admins.permissions.create', compact('parentPermission'));
    }

    public function store(Request $request)
    {
        try {

            $data = $request->all();
            DB::beginTransaction();
            $parentPermission = $this->permissionService->getPermission($data['moduleParent']);
            $this->permissionService->deleteByParentId($parentPermission->id);  
            foreach ($data['moduleChildren'] as $childrenPermission) {
                $dataChildrenPermission = [
                    'name' => $childrenPermission,
                    'parent_id' => $parentPermission->id,
                    'key_code' => $childrenPermission. '_' . $parentPermission->name,
                ];
                $this->permissionService->createChildrenPermissions($dataChildrenPermission);
            }
            DB::commit();

            return redirect()->back()->with('success', 'Create permission successfully');
        } catch (Exception $e) {

            DB::rollBack();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function getChildrenPermission(Request $request) 
    {
        $data = $request->all();
        $parentPermission = $this->permissionService->getPermission($data['id']);

        $childrenPermissions = $parentPermission->childrentPermissions->pluck('name')->toArray();

        return response()->json([
            'childrenPermissions' => $childrenPermissions,
        ]);
    }
}
