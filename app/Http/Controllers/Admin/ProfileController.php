<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return view('admins.profile.index', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        return response()->json([
            'status' => 200,
            'user' => $user,
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->all();
        $imagee = cloudinary()->upload($request->image->getRealPath());
        $data['public_id'] = $imagee->getPublicId();
        $data['image'] = $imagee->getSecurePath();
        $user->name = $data['name'];
        $user->email = $user->email;
        $user->phone = $data['phone'];
        $user->image = $data['image'];

        if ($data['password']) {
            $user->password = Hash::make($data['password']);
        }
        $user->update();
        return redirect()->back()->with('status', 'Update profile thành công');
    }
}
