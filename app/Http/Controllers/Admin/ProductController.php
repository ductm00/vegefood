<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Services\ProductService;
use App\Models\Product;
use App\Models\Category;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Services\TagService;

class ProductController extends Controller
{

    protected $productService;
    protected $tagservice;

    public function __construct(ProductService $productService, TagService $tagservice)
    {
        $this->productService = $productService;
        $this->tagservice = $tagservice;
        $this->middleware('can:product-list')->only('index');
        $this->middleware('can:product-edit')->only('edit','update');
        $this->middleware('can:product-delete')->only('destroy');
        $this->middleware('can:product-add')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $categories = Category::all();
        $tags = $this->tagservice->listTag();
        $products = Product::orderBy('products.id', 'DESC');
        if (isset($data['category_id'])) {
            $products = $products->where('category_id',$data['category_id']);
        } 

        if(isset($data['search'])) {
            $products = $products->where(function($query) use($data){
                $query->where('name','like','%'.$data['search'].'%')
                ->orWhere('code', $data['search'])
                ->orWhere('id', $data['search']);
            });
        }

        if ($request->status == 'instock') {
            $products = $products->where('quantity_instock','!=',0);
        }

        if ($request->status == 'outstock') {
            $products = $products->where('quantity_instock', 0);
        }

        if ($request->ajax()) {
            $output = '';
            $products = $products->get();
            if ($products) {
                foreach ($products as $key => $product) {
                    $output .= '<tr>
                    <td>' . $product->id . '</td>
                    <td>' . $product->name . '</td>
                    <td>' . $product->category->name . '</td>
                    <td><img width="80px" height="50px" src=' . $product->image . ' alt=""></td>
                    <td>' . $product->code . '</td>
                    <td>' . $product->quantity_instock . '</td>
                    <td>' . $product->unit . '</td>
                    <td>' . $product->price . '</td>
                    <td>' . $product->promotion_price . '</td>
                    <td>
                    <div style="display: flex">
                        <button style=" width: 35px; height: 35px;" type="button"
                            class="btn btn-primary btn-sm"><a style="color: white ;"
                                href=' . route("product.edit", [$product->id]) . '
                                class="ac-button">
                                <span class="far fa-edit"></span></a></button>

                        <form action=' . route("product.destroy", [$product->id]) . ' method="DELETE">
                            <button style=" width: 35px; height: 35px; margin-left: 5px" type="submit"
                                class="btn btn-danger btn-sm"
                                onclick="return confirm("Are you sure you want to delete this product ?)">
                                <span class="far fa-trash-alt"></span></button>
                        </form>
                    </div>
                </td>   
                    </tr>';
                }
            }

            return Response($output);
        }
        $products = $products->paginate(config('paginate.paginate'));
        return view('admins.products.index', compact('products', 'categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $imagee = cloudinary()->upload($request->image->getRealPath());
            $public_id = $imagee->getPublicId();
            $image = $imagee->getSecurePath();
            $data['public_id'] = $public_id;
            $data['image'] = $image;

            $product= Product::create($data);

            $product->tags()->attach($data['tag_id']);

        } catch (Exception $e) {
            Log::error($e);

            return redirect()->route('product.index')->with('error', $e->getMessage(). $e->getLine());
        }
        return redirect()->route('product.index')->with('success', 'Create product data successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        $tags = $this->tagservice->listTag();

        return view('admins.products.edit', compact('categories', 'product', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $data = $request->all();
            $product = Product::find($id);
            if (isset($data['image'])) {
                $imagee = cloudinary()->upload($request->file('image')->getRealPath());
                $data['image'] = $imagee->getSecurePath();
                $public_id = $imagee->getPublicId();
                $data['public_id'] = $public_id;
                cloudinary()->destroy($product->public_id);
                $product->fill($data)->save();
            } else {
                $product->name = $data['name'];
                $product->code = $data['code'];
                $product->quantity_instock = $data['quantity_instock'];
                $product->price = $data['price'];
                $product->promotion_price = $data['promotion_price'];
                $product->unit = $data['unit'];
                $product->description = $data['description'];
                $product->category_id = $data['category_id'];
                $product->save();
            }
            if(count($product->tags) != 0) {
                $product->tags()->detach();
            }
            $product->tags()->attach($data['tag_id']);
        } catch (Exception $e) {

            return redirect()->route('product.index')->with('error', 'Update category data failed !');
        }
        return redirect()->route('product.index')->with('success', 'Update category data successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return redirect()->route('product.index')->with('error', 'Delete product data failed !');
        } else {
            $product->delete();
            return redirect()->route('product.index')->with('success', 'Delete product data successfully');
        }
    }
}
