<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use Exception;

class CategoryController extends Controller
{

    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        $this->middleware('can:category-list')->only('index');
        $this->middleware('can:category-edit')->only('edit','update');
        $this->middleware('can:category-delete')->only('destroy');
        $this->middleware('can:category-add')->only('store');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $data = $request->all();
        $categories = $this->categoryService->showCategory($data);
        if ($request->ajax()) {
            $output = '';
            $categories = $categories->where(function ($query) use ($data) {
                $query->where('name', 'like', '%' . $data['search'] . '%')
                    ->orWhere('id', '' . $data['search']);
            })->get();
            if ($categories) {
                foreach ($categories as $key => $category) {
                    $output .= '<tr>
                            <td>' . $category->id . '</td>
                                            <td>' . $category->name . '</td>
                                            <td><img width="80px" height="50px" src=' . $category->image . ' alt=""></td>
                                            <td>
                                                <div style="display: flex">
                                                    <button style=" width: 35px; height: 35px;" type="button"
                                                        class="btn btn-primary btn-sm"><a style="color: white ;"
                                                            href=' . route("category.edit", [$category->id]) . '
                                                            class="ac-button">
                                                            <span class="far fa-edit"></span></a></button>

                                                    <form action=' . route('category.destroy', [$category->id]) . '
                                                        method="DELETE">
                                                        <button style=" width: 35px; height: 35px; margin-left: 5px" type="submit"
                                                            class="btn btn-danger btn-sm"
                                                            onclick="return confirm("Are you sure you want to delete this category ?")">
                                                            <span class="far fa-trash-alt"></span></button>
                                                    </form>
                                                </div>
                                            </td>
                            </tr>';
                }
            }
            return Response($output);
        }
        $categories = $categories->paginate(config('paginate.paginate'));
        return view('admins.categories.index', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {

        try {
            $data = $request->all();

            $this->categoryService->createCategory($data);
        } catch (Exception $e) {

            return redirect()->route('category.index')->with('error', 'Create category data failed');
        }
        return redirect()->route('category.index')->with('success', 'Create category data successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admins.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $this->categoryService->updateCategoryById($data, $id);
        } catch (Exception $e) {

            return redirect()->route('category.index')->with('error', 'Update category data failed !');
        }
        return redirect()->route('category.index')->with('success', 'Update category data successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if (!$category) {
            return redirect()->route('category.index')->with('error', 'Delete category data failed !');
        } else {
            $category->delete();
            return redirect()->route('category.index')->with('success', 'Delete category data successfully');
        }
    }
}
