<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrderExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Notify;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

class OrderController extends Controller
{
    public function __construct()
    {
       $this->middleware('can:order-list')->only('index');
       $this->middleware('can:order-edit')->only('update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
   
        $orders = Order::orderBy('id', 'DESC');
        $status = Order::status();
        if ($request->ajax()) {
            $output = '';
            $orders = $orders->where(function ($query) use ($data) {
                $query->where('id', '' . $data['search']);
            })->get();

            if ($orders) {
                foreach ($orders as $key => $order) {
                    $output .= '<tr>
                <td>' . $order->id . '</td>
                <td>' . $order->user->name . '</td>
                <td>' . $order->total . '</td>
                <td>' . $order->payment_method . '</td>
                <td>' . $order->status . '</td>
                <td>
                    <div style="display: flex">
                    <button style=" width: 35px; height: 35px;" type="button"
                                                        class="btn btn-info btn-sm"  data-toggle="modal" data-target="#detailModal' . $order->id . '" >
                                                            <span class="far fa-edit"></span></button>

                        <form action=' . route("order.destroy", [$order->id]) . ' method="DELETE">
                            <button style=" width: 35px; height: 35px; margin-left: 5px" type="submit"
                                class="btn btn-danger btn-sm"
                                onclick="return confirm("Are you sure you want to delete this category ?")">
                                <span class="far fa-trash-alt"></span></button>
                        </form>
                    </div>
                </td>
                </tr>';
                }
            }
            return Response($output);
        }
        $ordersxx = '';
        $order_tmp = $orders;
        $orders = $orders->paginate(config('paginate.paginate'));
        if (isset($data['order_id'])) {
            Notify::find($data['order_id'])->update(['status' => '1']);
            if(!in_array( $data['order_id'], $orders->pluck('id')->toArray())) {

                $ordersxx = Order::findOrFail($data['order_id']);
                $orders = $order_tmp->where('id', '!=', $data['order_id'])->paginate(config('paginate.paginate'));
            };
            
        }

        return view('admins.orders.index', compact('orders', 'status','ordersxx'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $order = Order::find($id);
        $order->status = $data['status'];
        $order->fill($data)->save();
        return redirect()->back();
    }

    public function orderExport() 
    {
        try {
            return Excel::download(new OrderExport, 'Orders.xlsx');

        } catch (Exception $e) {

            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
