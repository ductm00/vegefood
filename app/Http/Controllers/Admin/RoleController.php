<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Services\RoleService;
use App\Services\PermissionService;
use Exception;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $roleService;
    protected $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
        $this->middleware('can:role-list')->only('index');
        $this->middleware('can:role-edit')->only('edit','update');
        $this->middleware('can:role-delete')->only('destroy');
        $this->middleware('can:role-add')->only('store', 'create');
    }

    public function index(Request $request)
    {
        $data = $request->all();
        
        $roles = $this->roleService->getRoles($data);

        return view('admins.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentPermissions = $this->permissionService->getParentPermissions();

        return view('admins.roles.create', compact('parentPermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $data = $request->all();
            DB::beginTransaction();

            $this->roleService->saveRole($data);

            DB::commit();

            return redirect()->route('role.index')->with('success', 'Create role successfuly');
        } catch (Exception $e) {
            DB::rollBack();
            
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->roleService->getRoleById($id);
        $parentPermissions = $this->permissionService->getParentPermissions();

        return view('admins.roles.edit', compact('role', 'parentPermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $data = $request->all();
            DB::beginTransaction();

            $this->roleService->updateRole($data, $id);

            DB::commit();

            return redirect()->route('role.index')->with('success', 'Update role successfuly');
        } catch (Exception $e) {
            DB::rollBack();
            
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $this->roleService->deleteRoleById($id);
            DB::commit();

            return redirect()->route('role.index')->with('success', 'Delete role successfully');
        } catch (Exception $e) {
            DB::rollBack();
            
            return redirect()->route('role.index')->with('error', $e->getMessage());
        }
    }
}
