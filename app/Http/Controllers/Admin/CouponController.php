<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CouponRequest;
use App\Models\Coupon;
use Carbon\Carbon;
use Exception;

class CouponController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:coupon-list')->only('index');
        $this->middleware('can:coupon-edit')->only('edit','update');
        $this->middleware('can:coupon-delete')->only('destroy');
        $this->middleware('can:coupon-add')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $coupons = Coupon::orderBy('coupons.code','DESC');
        if ($request->ajax()) {
            $output = '';
            $coupons = $coupons->where(function($query) use($data){
                $query->orWhere('code',''.$data['search']);
            })->get();
        if ($coupons) {
            foreach ($coupons as $key => $coupon) {
                $output .= '<tr>
                <td>'.$coupon->code.'</td>
                <td>'.$coupon->value .'</td>
                <td>'. $coupon->start_date .'</td>
                <td>'.$coupon->end_date .'</td>
                <td>
                    <div style="display: flex">
                        <button style=" width: 35px; height: 35px;" type="button"
                            class="btn btn-primary btn-sm"><a style="color: white ;"
                                href='.route("coupon.edit", [$coupon->code]).'
                                class="ac-button">
                                <span class="far fa-edit"></span></a></button>

                        <form action='.route("coupon.destroy", [$coupon->code]).' method="DELETE">
                            <button style=" width: 35px; height: 35px; margin-left: 5px" type="submit"
                                class="btn btn-danger btn-sm"
                                onclick="return confirm("Are you sure you want to delete this category ?")">
                                <span class="far fa-trash-alt"></span></button>
                        </form>
                    </div>
                </td>
                </tr>';
            }
        }
        return Response($output);
    }
        $coupons = $coupons->paginate(config('paginate.paginate'));
        
        return view('admins.coupons.index',compact('coupons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        try{

            $data = $request->all();
            $coupon = new Coupon();
            $coupon = $coupon->fill($data)->save();

            return redirect()->route('coupon.index')->with('success','Create coupon data successfully');
        } catch(Exception $e) {

            return redirect()->route('coupon.index')->with('error', $e->getMessage());
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code)
    {
        $coupon = Coupon::find($code);

        return view('admins.coupons.edit',compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code)
    {
        try { 
           
            $data = $request->all();
            $coupon = Coupon::find($code); 
            $coupon->fill($data)->save();
        } catch(Exception $e) {

            return redirect()->route('coupon.index')->with('error','Update coupon data failed !');
        }
        return redirect()->route('coupon.index')->with('success','Update coupon data successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code)
    {
        $coupon = Coupon::find($code);
        if (!$coupon) {
            
            return redirect()->route('coupon.index')->with('error','Delete coupon data failed !');
        }else{
            $coupon->delete();

            return redirect()->route('coupon.index')->with('success','Delete coupon data successfully');
        }
    }
}
