<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Order_Product;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
// use Carbon\Carbon;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order_waiting = Order::where('status', 'Waiting')->count();
        $order_confirmed = Order::where('status', 'Confirmed')->count();
        $order_delivering = Order::where('status', 'Delivering')->count();
        $order_received = Order::where('status', 'Received')->count();
        $order_canceller = Order::where('status', 'Cancelled')->count();
        // $best_sell_product = Order_Product::groupBy('product_id');
        $best_selling_products = Order_Product::select('product_id', DB::raw('SUM(quantity) AS "quantity"'))
            ->groupBy('product_id')
            ->orderBy('quantity','DESC')
            ->limit(5)
            ->get();

        $worst_selling_products = Order_Product::select('product_id', DB::raw('SUM(quantity) AS "quantity"'))
            ->groupBy('product_id')
            ->orderBy('quantity','ASC')
            ->limit(5)
            ->get();
        // SELECT * ,
        // SUM(quantity) AS "Tong"
        // FROM `order_product`
        // GROUP BY product_id
        // ORDER BY quantity DESC;');
        // dd($best_sell_product);
        $pro_fruit = Product::where('category_id', '3')->count();
        $pro_vege = Product::where('category_id', '2')->count();
        $pro_juice = Product::where('category_id', '1')->count();
        $pro_grain = Product::where('category_id', '4')->count();
        $total_product = Product::all()->count();


        // dd($count_products);
        // dd($order_waiting);
        return view('admins.dashboard', compact('best_selling_products','worst_selling_products', 'order_waiting', 'order_confirmed', 'order_delivering', 'order_received', 'order_canceller', 'pro_fruit', 'pro_vege', 'pro_juice', 'pro_grain', 'total_product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function filterByDate(Request $request)
    {
        $data = $request->all();
        $fromdate = $data['fromdate'];
        $todate = $data['todate'];

        $get = Order::whereBetween('created_at', [$fromdate, $todate])->orderBy('created_at', 'ASC')->get();

        foreach ($get as $key => $val) {
            $chart_data[] = [
                'total' => $val->total,
            ];
        }
        $data = json_encode($chart_data);
        echo $data;
    }
    // public function dayOrder(Request $request)
    // {
    //     $data = $request->all();
    //     $created_at = $_GET['created_at'];
    //     $total = Order::where('created_at',$created_at)->orderBy('created_at','DESC')->get();
    //     return view('admins.dashboard', compact('total'));
    // }

    public function dashboardFilter(Request $request)
    {
        $data = $request->all();

        $dauthangnay = Carbon::now('Asia/Ho_Chi_Minh')->startOfMonth()->toDateString();
        $dautuannay = Carbon::now('Asia/Ho_Chi_Minh')->startOfWeek()->toDateString();
        $daunamnay = Carbon::now('Asia/Ho_Chi_Minh')->startOfYear()->toDateString();
        $dautuannay = Carbon::now('Asia/Ho_Chi_Minh')->startOfWeek()->toDateString();
        $dauthangtruoc = Carbon::now('Asia/Ho_Chi_Minh')->subMonth()->startOfMonth()->toDateString();
        $cuoithangtruoc = Carbon::now('Asia/Ho_Chi_Minh')->subMonth()->endOfMonth()->toDateString();
        $sub7days = Carbon::now('Asia/Ho_Chi_Minh')->subDays(7)->toDateString();
        $sub365days = Carbon::now('Asia/Ho_Chi_Minh')->subDays(365)->toDateString();
        $sub30days = Carbon::now('Asia/Ho_Chi_Minh')->subDays(30)->toDateString();
        $now = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $tomorrow = Carbon::tomorrow('Asia/Ho_Chi_Minh')->toDateString();

        if ($data['dashboard_value'] == "thisweek") {
            $get = Order::whereBetween('created_at', [$dautuannay, $tomorrow])->orderBy('created_at', 'ASC')->get();
        } elseif ($data['dashboard_value'] == "thismonth") {
            $get = Order::whereBetween('created_at', [$dauthangnay, $tomorrow])->orderBy('created_at', 'ASC')->get();
        } else {
            $get = Order::whereBetween('created_at', [$daunamnay, $tomorrow])->orderBy('created_at', 'ASC')->get();
        }

        foreach ($get as $key => $val) {
            $format_date = Carbon::parse($val->created_at)->format('d/m/Y');
            $chart_data[] = [
                'total' => $val->total,
                'created_at' => $format_date,
            ];
        }
        $data = json_encode($chart_data);
        echo $data;
    }
}
