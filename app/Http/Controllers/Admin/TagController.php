<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TagService;
use App\Http\Requests\TagRequest;
use Exception;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $tagService;

    public function __construct(TagService $tagService)
    {
        $this->tagService = $tagService;
        $this->middleware('can:tag-list')->only('index');
        $this->middleware('can:tag-edit')->only('edit','update');
        $this->middleware('can:tag-delete')->only('destroy');
        $this->middleware('can:tag-add')->only('store');
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $tags = $this->tagService->showTag($data);

        return view('admins.tags.index', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        try {
            $data = $request->all();
            $this->tagService->saveTag($data);

            return redirect()->route('tag.index')->with('success', 'Create tag successfully!');
        } catch (Exception $e) {

            return redirect()->route('tag.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $tag = $this->tagService->findTagBySlug($slug);

        return view('admins.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, $slug)
    {
        try {
            $data = $request->all();

            $this->tagService->updateTag($data, $slug);

            return redirect()->route('tag.index')->with('success', 'Update this tag successfully');
        } catch (Exception $e) {

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $this->tagService->deleteTagById($id);

            DB::commit();

            return redirect()->back()->with('success', 'Delete tag successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', $e->getMessage());
        };
    }
}
