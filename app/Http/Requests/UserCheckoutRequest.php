<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'receiver_name' => 'required',
           'receiver_phone' => 'required',
           'receiver_email' => 'required',
           'receiver_city' => 'required',
           'receiver_district' => 'required',
           'receiver_village' => 'required',
           'receiver_address' => 'required',
           'payment_method' => 'required',
        ];
    }
}
