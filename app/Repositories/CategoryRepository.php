<?php

namespace App\Repositories;

use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use App\Repositories\AbstractRepository;

class CategoryRepository extends AbstractRepository
{
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    public function showCategory(array $data)
    {
        if (isset($data['search'])) {
            $categories = $this->model->where(function ($query) use ($data) {
                $query->orWhere('name', 'like', '%' . $data['search'] . '%')
                    ->orWhere('id', '' . $data['search']);
            });
            return $categories;
        }
        return $this->model->orderBy('categories.id', 'DESC');
    }

    public function createCategory($data)
    {
        try{
        $category = new Category();
            $imagee = cloudinary()->upload($data['image']->getRealPath());
            $public_id = $imagee->getPublicId();
            $image = $imagee->getSecurePath();
            $data['public_id'] = $public_id;
            $data['image'] = $image;
            $category->fill($data)->save();
            } catch(Exception $e) {
                return ["msg" => "Create Category failed"];
            }
            return ["msg" => "Create Category Successfully"];
    }

    public function updateCategoryById($data , $id){
        $category = Category::find($id);
        if (isset($data['image'])) {
            $imagee = cloudinary()->upload($data['image']->getRealPath());
            $data['image'] = $imagee->getSecurePath();
            $public_id = $imagee->getPublicId();
            $data['public_id'] = $public_id;
            cloudinary()->destroy($category->public_id);
            $category->fill($data)->save();
        } else {
            $category->name = $data['name'];
            $category->slug = $data['slug'];
            $category->save();
        }
    }

    public function getIdBySlug($categorySlug)
    {
        return $this->model->where('slug', 'like', $categorySlug)->first()->id;
    }
}
