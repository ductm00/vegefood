<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\AbstractRepository;

class RoleRepository extends AbstractRepository
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    public function getRoleById($role_id)
    {
        return $this->model->findOrFail($role_id);
    }

    public function saveRole($data)
    {

        $role = $this->model->create($data);
        $role->permissions()->attach($data['permission_id']);
    }
    public function updateRole($data, $id)
    {
        $role = $this->model->findOrFail($id);
        $role->update($data);
        $role->permissions()->detach();
        $role->permissions()->attach($data['permission_id']);
    }

    public function getRoles($data)
    {
        $roles = $this->model->orderBy('id', 'DESC');

        if (isset($data['search'])) {
            $roles = $this->model->where(function ($query) use ($data) {
                $query->where('name', 'like', '%' . $data['search'] . '%')
                    ->orWhere('display_name', 'like', '%' . $data['search'] . '%')
                    ->orWhere('id', $data['search']);
            });
        }

        $roles = $roles->paginate(config('paginate.paginate'));
        return $roles;
    }

    public function listRoles()
    {
        return $this->model->orderBy('id', 'DESC')->get();
    }
    public function deleteRoleById($id)
    {
        $role = $this->model->find($id);
        if(count($role->users) != 0) {
            $role->users()->detach();
        }
        if(count($role->permissions) != 0) {
            $role->permissions()->detach();
        }

        return $role->delete();
    }
}
