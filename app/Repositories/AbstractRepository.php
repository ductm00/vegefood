<?php

namespace App\Repositories;

use App\Repositories\InterfaceRepository;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Traits\QueryTrait;

abstract class AbstractRepository implements InterfaceRepository
{
    use QueryTrait;
    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model = '';

    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
