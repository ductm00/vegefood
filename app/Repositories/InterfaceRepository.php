<?php

/**
 * @method Model get();
 * @method Model create(array $data)
 * @method Model update(Model $model, array $data = [])
 * @method Model delete(Model $model)
 * @method Model paginate(int $limit = 30)
 */


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;


interface InterfaceRepository
{
    /**
     * @return Model|null
     */
    public function get();

    /**
     * @param array $data
     *
     * @return Model|null
     */
    public function create(array $data);

    /**
     * @param Model
     * @param array $data
     *
     * @return Model|null
     */
    public function update(Model $model, array $data = []);

    /**
     * @param Model
     *
     * @return Model|null
     */
    public function delete(Model $model);

    /**
     * @param int $limit
     *
     * @return Model|null
     */
    public function paginate(int $limit = 30);

    /**
     * @param string $relationship
     * @param array $ids=null
     *
     * @return Model|null
     */
    public function detach(string $relationship, array $ids = null);

    /**
     * @param string $relationship
     * @param array $ids=null
     *
     * @return Model|null
     */
    public function attach(string $relationship, array $ids = null);

    /**
     * @param string $relationship
     * @param array $ids=null
     *
     * @return Model|null
     */
    public function sync(string $relationship, array $ids = null);

    /**
     * @param Model $model
     * @param string $relationship
     * @param array $ids=null
     *
     * @return Model|null
     */
    public function syncWithModel(Model $model, string $relationship, array $ids = null);

    /**
     * @param Model $model
     * @param string $relationship
     * @param array $ids=null
     *
     * @return Model|null
     */
    public function detachWithModel(Model $model, string $relationship, array $ids = null);

    /**
     * @param Model $model
     * @param string $relationship
     * @param array $ids=null
     *
     * @return Model|null
     */
    public function attachWithModel(Model $model, string $relationship, array $ids = null);
}
