<?php

namespace App\Repositories;

use App\Models\Tag;
use App\Repositories\AbstractRepository;


class TagRepository extends AbstractRepository
{
    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }

    public function showTag($data)
    {
        $tags = $this->model->orderBy('id', 'DESC');
        if (isset($data['search'])) {
            $tags = $this->model->where(function ($query) use ($data) {
                $query->where('name', 'like', '%' . $data['search'] . '%')
                    ->orWhere('slug', 'like', '%' . $data['search'] . '%')
                    ->orWhere('id', $data['search']);
            });
        }

        $tags = $tags->paginate(config('paginate.paginate'));

        return $tags;
    }

    public function listTag()
    {
        return $this->model->orderBy('id', 'DESC')->get();
    }

    public function saveTag($data)
    {
        return $this->model->create($data);
    }

    public function getProductsByTag($tagId)
    {
        return $this->model->find($tagId)->products;
    }

    public function deleteTagById($id)
    {
        $tag = $this->model->find($id);

        if ($tag) {
            if ($tag->products->count() != 0) {

                $tag->products->detach();
            }
            $tag->delete();
        }
    }

    public function findTagBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    public function updateTag($data, $slug)
    {
        $tag = $this->model->where('slug', $slug)->first();

        return $tag->update($data);
    }
}
