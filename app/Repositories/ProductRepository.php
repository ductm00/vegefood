<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\AbstractRepository;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;


class ProductRepository extends AbstractRepository
{
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    public function showProduct()
    {
        return $this->model->all();
    }

    public function getTagsByProductId($producId) 
    {
        return $this->model->find($producId)->tags;
    }
}
