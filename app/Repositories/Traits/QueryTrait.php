<?php

namespace App\Repositories\Traits;

use Illuminate\Database\Eloquent\Model;

trait QueryTrait
{
    public function get()
    {
        return $this->model->get();
    }

    /**
     * @param int $id
     *
     * @return Model|null
     */
    public function findById(int $id): ?Model
    {
        return $this->model->find($id);
    }

    public function create(array $data)
    {
        foreach ($data as $k => $v) {
            $this->model->{$k} = $v;
        }
        return $this->model->save();
    }

    public function update(Model $model, array $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $model->{$k} = $v;
            }
        }
        return $model->save();
    }

    public function delete(Model $model)
    {
        return $model->delete();
    }

    public function paginate($limit = 30)
    {
        return $this->model->paginate($limit);
    }

    public function detach(string $relationship, array $ids = null)
    {
        return $this->model->{$relationship}()->detach($ids);
    }

    public function attach(string $relationship, array $ids = null)
    {
        return $this->model->{$relationship}()->attach($ids);
    }

    public function sync(string $relationship, array $ids = null)
    {
        return $this->model->{$relationship}()->sync($ids);
    }

    public function syncWithModel(Model $model, string $relationship, array $ids = null)
    {
        return $model->{$relationship}()->sync($ids);
    }

    public function detachWithModel(Model $model, string $relationship, array $ids = null)
    {
        return $model->{$relationship}()->detach($ids);
    }

    public function attachWithModel(Model $model, string $relationship, array $ids = null)
    {
        return $model->{$relationship}()->attach($ids);
    }
}
