<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Role;
use App\Repositories\AbstractRepository;


class UserRepository extends AbstractRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function showUser($data)
    {
       $users = $this->model->orderBy('id', 'DESC');
       if(isset($data['search'])) {
        $users = $this->model->where(function ($query) use ($data) {
            $query->where('name', 'like', '%' . $data['search'] . '%')
                ->orWhere('email', 'like', '%' . $data['search'] . '%')
                ->orWhere('phone', 'like', '%' . $data['search'] . '%')
                ->orWhere('id', $data['search']);
        });
       }

       if(isset($data['role_id'])) {
        $role_id = Role::find($data['role_id'])->users->pluck('id')->toArray();
        $users = $this->model->whereIn('id', $role_id);
    }

       $users = $users->paginate(config('paginate.paginate'));
       return $users;
    }

    public function getUserById($userId)
    {
        return $this->model->findOrFail($userId);
    }

}
