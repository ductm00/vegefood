<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Repositories\AbstractRepository;


class PermissionRepository extends AbstractRepository
{
    public function __construct(Permission $model)
    {
        parent::__construct($model);
    }

    public function getPermission($id)
    {
        return $this->model->find($id);
    }

    public function getParentPermissions() {
        
        return $this->model->where('parent_id','1')->orderBy('id', 'ASC')->get();
    }

    public function createChildrenPermissions($data)
    {
        return $this->model->create($data);
    }
    
    public function deleteByParentId($parentId)
    {
        return $this->model->where('parent_id', $parentId)->delete();
    }

}
