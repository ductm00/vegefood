<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getParentPermissions() 
    {
        return $this->permissionRepository->getParentPermissions();
    }

    public function getPermission($id) 
    {
        return $this->permissionRepository->getPermission($id);
        
    }
    
    public function createChildrenPermissions($data) 
    {
        return $this->permissionRepository->createChildrenPermissions($data);      
    }

    public function deleteByParentId($parentId) 
    {
        return $this->permissionRepository->deleteByParentId($parentId);
    }
}
