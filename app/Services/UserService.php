<?php

namespace App\Services;

use App\Repositories\UserRepository;
class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function showUser($data)
    {
        return $this->userRepository->showUser($data);
    }

    public function getUserById($userId) 
    {
        return $this->userRepository->getUserById($userId);
    }
}
