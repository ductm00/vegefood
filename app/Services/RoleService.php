<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getRoleById($role_id)
    {
        return $this->roleRepository->getRoleById($role_id);
    }

    public function saveRole($data) 
    {
        $this->roleRepository->saveRole($data);
    }

    public function updateRole($data, $id) 
    {
        $this->roleRepository->updateRole($data, $id);
    }

    public function getRoles($data) 
    {
        return $this->roleRepository->getRoles($data);
    }

    public function listRoles()
    {
        return $this->roleRepository->listRoles();
    }

    public function deleteRoleById($id)
    {
        return $this->roleRepository->deleteRoleById($id);
    }
}
