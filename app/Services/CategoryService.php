<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function showCategory($data)
    {
        return $this->categoryRepository->showCategory($data);
    }

    public function createCategory($data)
    {
        return $this->categoryRepository->createCategory($data);
    }

    public function updateCategoryById($data , $id)
    {
        return $this->categoryRepository->updateCategoryById($data , $id);
    }
    public function getIdBySlug($categorySlug)
    {
        return $this->categoryRepository->getIdBySlug($categorySlug);
    }
}
