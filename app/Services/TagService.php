<?php

namespace App\Services;

use App\Repositories\TagRepository;
class TagService
{
    protected $tagRepository;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function showTag($data)
    {
        return $this->tagRepository->showTag($data);
    }

    public function listTag()
    {
        return $this->tagRepository->listTag();
    }

    public function saveTag($data) 
    {
        return $this->tagRepository->saveTag($data);
    }

    public function getProductsByTag($tagId) {
        return $this->tagRepository->getProductsByTag($tagId);
    }

    public function deleteTagById($id)
    {
        return $this->tagRepository->deleteTagById($id);
    }

    public function findTagBySlug($slug) 
    {
        return $this->tagRepository->findTagBySlug($slug);
    }

    public function updateTag($data, $slug)
    {
        return $this->tagRepository->updateTag($data, $slug);
    }
}
